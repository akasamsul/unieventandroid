<h1> Mobile Application for Information and Notification (UNIEVENT) </h1>

<p>Event finder mobile application for University Event </p>

<p><b> Overview this application </b></p>
<p> UNIEVENT is a mobile application platform that collect and disseminate information or called as an event finder for University student who are wanted to find an event for themselves. Organizer also can publish their event in this platform and can view who are the participate in their event. Student can find any event that meet their preferences and can join them. Participate will received alert notification about up coming event once they have register to the event. </p>

<p><i><b>Features - Facebook Integration, Alert Notification, Recommendation system, Flash news</b></i></p>

![unievent_all](/uploads/ba7015e3532badda7eb9d2459b2fd0ec/unievent_all.png)


<p><b><i> Facebook Integration </i></b></p>
<p> User can login through their Facebook account or register using their email within the application, once user done registering to the application user will received an email conformation in their email.</p>


<p><b><i>Recommendation system</i></b></p>
<p> By using collaborative recommendation system concept this application will recommend the user any event that user might liked to join. To explained this concept, this recommendation system similar to the Nextflix recommendation system. Let say if user A liked Marvel movie and Harry Potter movie, and user B liked DC movie and also liked Harry Potter movie, then application will recommend to the user B Marvel movie, because these two users are consider as a same user or has same preferences</p>

<p><b><i>Alert Notification </i></b></p>
<p>Once user has register to the any event that are available in this app, user will received alert notification 24 hour before the event occur</p>

<p><b><i>Flash News </i></b></p>
<p>Flash news concept is similar to the Instagram Story, the story will only available within 24 hour. But for this application it will be only available within 3 days only. This flash news is for the organizer to promote their event or to make an announcement update about anythings they want. Once their post this flash news, this post will only available to user within 3 days </p>

<p><b><i>User Features </i></b></p>
<ul>
    <li>user can register to any event that are available </li>
    <li>user can get recommendation event, that are provided in this application </li>
    <li>user can search any event that meet their preferences </li>
    <li>user can view up coming and past event </li>
    <li>user can view flash news that are updated by all organizer</li>
    <li>user can view the comment from organizer once they register to the event </li>
</ul>

<p><b><i>Organizer Features </i></b></p>
 <ul>
    <li>organizer can publish or create any event in this platform </li>
    <li>organizer can view all the participate that are joined the event </li>
    <li>organizer can create flash news</li>
    <li>organizer can update and delete the event</li>
    <li>organizer can write a comment to the event</li>
 </ul>