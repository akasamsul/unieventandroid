package com.example.unievent.Model;

public class User {

    private String name;
    private String fbid;
   private String role_type;
   private String email;
   private String password;
   private String Token;
   private String id;



    public User() {
    }




    public User(String name, String fbid, String role_type, String email, String password) {
        this.name = name;
        this.fbid = fbid;
        this.role_type = role_type;
        this.email = email;
        this.password = password;
    }

    public User(String name, String fbid, String role_type, String email, String password, String token, String id) {
        this.name = name;
        this.fbid = fbid;
        this.role_type = role_type;
        this.email = email;
        this.password = password;
        Token = token;
        this.id = id;
    }

    public User(String name, String token, String id, String email) {
        this.name = name;
        Token = token;
        this.id = id;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFbid() {
        return fbid;
    }

    public void setFbid(String fbid) {
        this.fbid = fbid;
    }

    public String getRole_type() {
        return role_type;
    }

    public void setRole_type(String role_type) {
        this.role_type = role_type;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
