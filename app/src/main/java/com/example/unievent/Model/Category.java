package com.example.unievent.Model;

public class Category {

    private String category;
    private String id;
    private String value1;
    private int value;

    public Category(String category, String id) {
        this.category = category;
        this.id = id;
    }



//    public Category(String category, String value1) {
//        this.category = category;
//        this.value1 = value1;
//    }

//    public Category(String category, String id, String value) {
//        this.category = category;
//
//        this.value1 = value;
//    }

    public Category(String category, int value) {
        this.category = category;
        this.value = value;
    }

    //    public Category(String category, int i) {
//    }

    public Category(String category) {
        this.category = category;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue1() {
        return value1;
    }

    public void setValue1(String value1) {
        this.value1 = value1;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
