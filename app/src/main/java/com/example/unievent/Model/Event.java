package com.example.unievent.Model;

public class Event {

    private String id;
    private String name;
    private String description;
    private String date;
    private String venue;
    private String location;
    private String image;
    private String user_id;
    private String dateddd;
    private String timeddd;
    private String category2;
    private String comment;
    private String organizer;

    public Event(String id, String name, String description, String date, String venue, String location, String image, String dateddd, String timeddd, String category2, String comment, String organizer) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.date = date;
        this.venue = venue;
        this.location = location;
        this.image = image;
        this.dateddd = dateddd;
        this.timeddd = timeddd;
        this.category2 = category2;
        this.comment = comment;
        this.organizer = organizer;
    }



    public Event(String id, String name, String description, String date, String venue, String location, String image, String dateddd, String timeddd, String category2) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.date = date;
        this.venue = venue;
        this.location = location;
        this.image = image;
        this.dateddd = dateddd;
        this.timeddd = timeddd;
        this.category2 = category2;
    }

    public Event(String id, String name, String description, String date, String venue, String location, String image, String dateddd, String timeddd, String category2,String organizer) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.date = date;
        this.venue = venue;
        this.location = location;
        this.image = image;
        this.dateddd = dateddd;
        this.timeddd = timeddd;
        this.category2 = category2;
        this.organizer = organizer;
    }

    public String getCategory2() {
        return category2;
    }

    public void setCategory2(String category2) {
        this.category2 = category2;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getOrganizer() {
        return organizer;
    }

    public void setOrganizer(String organizer) {
        this.organizer = organizer;
    }

    public Event(String id, String name, String description, String date, String venue, String location, String image, String dateddd, String timeddd) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.date = date;
        this.venue = venue;
        this.location = location;
        this.image = image;
        this.dateddd = dateddd;
        this.timeddd = timeddd;
    }

    public Event(String id, String name, String description, String date, String venue, String image, String dateddd, String timeddd) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.date = date;
        this.venue = venue;
        this.image = image;
        this.dateddd = dateddd;
        this.timeddd = timeddd;
    }


    public Event(String id, String user_id) {
        this.id = id;
        this.user_id = user_id;
    }

    public Event() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Event(String id, String name, String description, String date, String venue, String location, String image) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.date = date;
        this.venue = venue;
        this.location = location;
        this.image = image;
    }



    public Event(String name, String description, String date, String venue, String location, String image) {
        this.name = name;
        this.description = description;
        this.date = date;
        this.venue = venue;
        this.location = location;
        this.image = image;
    }

    public String getDateddd() {
        return dateddd;
    }


//    public ArrayList getCategory() {
//        return category;
//    }
//
//    public void setCategory(ArrayList category) {
//        this.category = category;
//    }




    public void setDateddd(String dateddd) {
        this.dateddd = dateddd;
    }

    public String getTimeddd() {
        return timeddd;
    }

    public void setTimeddd(String timeddd) {
        this.timeddd = timeddd;
    }

    public String getImage() {
        return image;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getDate() {
        return date;
    }

    public String getVenue() {
        return venue;
    }

    public String getLocation() {
        return location;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
