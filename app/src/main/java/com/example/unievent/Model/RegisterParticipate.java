package com.example.unievent.Model;

public class RegisterParticipate {

    private String name;
    private String matrix;
    private String course;
    private String phone;

    public RegisterParticipate(String name, String matrix, String course, String phone) {
        this.name = name;
        this.matrix = matrix;
        this.course = course;
        this.phone = phone;
    }

    public RegisterParticipate(String name, String matrix) {
        this.name = name;
        this.matrix = matrix;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMatrix() {
        return matrix;
    }

    public void setMatrix(String matrix) {
        this.matrix = matrix;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
