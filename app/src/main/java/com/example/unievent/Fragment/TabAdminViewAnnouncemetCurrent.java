package com.example.unievent.Fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.unievent.Adapter.MyAdapterAnnouncement;
import com.example.unievent.Details.DetailActivity_Announcement_admin;
import com.example.unievent.Model.Announcement;
import com.example.unievent.Model.Constants;
import com.example.unievent.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabAdminViewAnnouncemetCurrent extends Fragment implements MyAdapterAnnouncement.OnItemClickListener{


    public TabAdminViewAnnouncemetCurrent() {
        // Required empty public constructor
    }


    private static final String URL_DATA = Constants.BASE_URL+"/api/displayAnnoucmentsForAdmin";

    private RecyclerView recyclerView;

    private MyAdapterAnnouncement adapter;

    private List<Announcement> announcementList;
    SharedPreferences sharedPreferences;
    String id,email,token,Ename;

    View rootView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       rootView = inflater.inflate(R.layout.fragment_tab_admin_view_announcemet_current, container, false);

        sharedPreferences = getContext().getSharedPreferences("admin", Context.MODE_PRIVATE);
        id = sharedPreferences.getString("adminID","");
        email = sharedPreferences.getString("adminEmail","");
        token = sharedPreferences.getString("adminToken","");
        Ename = sharedPreferences.getString("adminName","");


        recyclerView = rootView.findViewById(R.id.recyclerViewAnnouncement);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        announcementList = new ArrayList<>();




        loadRecyclerViewData();

        return rootView;
    }

    private void loadRecyclerViewData(){
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("LOADING DATA...");
        progressDialog.show();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_DATA, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                progressDialog.dismiss();
                try {
                    JSONArray j = new JSONArray(response);

                    for (int i =0; i<j.length(); i++)
                    {
                        JSONObject obj= j.getJSONObject(i);

                        Announcement announcement = new Announcement(
                                obj.getString("id"),
                                obj.getString("title"),
                                obj.getString("description")

                        );
                        announcementList.add(announcement);

                    }

                    adapter = new MyAdapterAnnouncement(announcementList,getApplicationContext());
                    recyclerView.setAdapter(adapter);
                    adapter.setOnItemClickListener(TabAdminViewAnnouncemetCurrent.this);

                } catch (Exception e) {
                    e.printStackTrace();
                }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(getContext(),error.getMessage(),Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();

                params.put("user_id", id);

                return params;

            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }


    @Override
    public void onItemClick(int position) {
        Intent i = new Intent(getActivity(), DetailActivity_Announcement_admin.class);
        Announcement  clickedItem = announcementList.get(position);

        i.putExtra("id",clickedItem.getId());
        i.putExtra("title",clickedItem.getTitle());
        i.putExtra("description",clickedItem.getDescription());



//        sharedPreferences = getSharedPreferences("event", Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//
//        editor.putString("event_id",clickedItem.getId());
//        editor.putString("timed",clickedItem.getTimeddd());
//        editor.apply();

        startActivity(i);
    }
}
