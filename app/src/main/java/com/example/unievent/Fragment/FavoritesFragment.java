package com.example.unievent.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.unievent.Adapter.SectionPagerAdapter;
import com.example.unievent.R;
import com.google.android.material.tabs.TabLayout;

public class FavoritesFragment extends Fragment  {

    View myFragment;
    ViewPager viewPager;
    TabLayout tabLayout;

//    private static final String URL_DATA = Constants.BASE_URL+"/api/userHasJoinEvent";
//
//    private RecyclerView recyclerView;
//    private MyAdapterJoinEvent myAdapterJoinEvent;
//    private List<Event> eventListJoin;
//
//    SharedPreferences sharedPreferences;
//
//    String id,email,name,token;
//    String fbid,fbeamail,fbname,fuid;
//    String ucheck,fcheck;

    View rootView;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_favorites,container,false);

        viewPager = rootView.findViewById(R.id.viewPager);
        tabLayout = rootView.findViewById(R.id.tabLayout);


//        sharedPreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
//        id = sharedPreferences.getString("userID","");
////        email = sharedPreferences.getString("userEmail","");
////        name = sharedPreferences.getString("userName","");
////        token = sharedPreferences.getString("userToken","");
//        ucheck = sharedPreferences.getString("check","");
////
////
//        sharedPreferences = getActivity().getSharedPreferences("userFacebook", Context.MODE_PRIVATE);
//        fuid = sharedPreferences.getString("ID","");
////        fbeamail = sharedPreferences.getString("fbEmail","");
////        fbname = sharedPreferences.getString("fbName","");
////        fbid = sharedPreferences.getString("fbID","");
//        fcheck = sharedPreferences.getString("check","");
//
//
//        recyclerView = rootView.findViewById(R.id.recyclerViewListEventJoin);
//        recyclerView.setHasFixedSize(true);
//        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
//
//
//        eventListJoin = new ArrayList<>();
//        loadRecyclerViewDataJoin();

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setUpViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    private void setUpViewPager(ViewPager viewPager) {

        SectionPagerAdapter adapter = new SectionPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new TabCurrentFragment(), "Upcoming Event");
        adapter.addFragment(new TabPastFragment(),"Past Event");

        viewPager.setAdapter(adapter);
    }

//    private void loadRecyclerViewDataJoin(){
//        final ProgressDialog progressDialog = new ProgressDialog(getContext());
//        progressDialog.setMessage("LOADING DATA...");
//        progressDialog.show();
//
//
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_DATA, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//
//                progressDialog.dismiss();
//                try {
//                    JSONArray j = new JSONArray(response);
//
//                    for (int i =0; i<j.length(); i++)
//                    {
//                        JSONObject obj= j.getJSONObject(i);
//
//                        JSONArray obj2 = obj.getJSONArray("categories");
//                        ArrayList category = new ArrayList();
//                        String cate ="";
//                        for(int k =0; k<obj2.length(); k++)
//                        {
//                            JSONObject objCategory = obj2.getJSONObject(k);
//                            String catName =  objCategory.getString("category");
//
//
//                            cate += catName+", ";
//
//
//                        }
//                        Event event = new Event(
//                                obj.getString("id"),
//                                obj.getString("name"),
//                                obj.getString("description"),
//                                obj.getString("date"),
//                                obj.getString("venue"),
//                                obj.getString("location"),
//                                obj.getString("image"),
//                                obj.getString("dated"),
//                                obj.getString("timed"),
//                                cate
//
//
//                        );
//
//                        Log.i("fdsafd",cate);
//                        eventListJoin.add(event);
//
//                    }
//
//                    myAdapterJoinEvent = new MyAdapterJoinEvent(eventListJoin,getContext());
//                    recyclerView.setAdapter(myAdapterJoinEvent);
//                    myAdapterJoinEvent.setOnClickListener(FavoritesFragment.this);
//
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                progressDialog.dismiss();
//                Toast.makeText(getContext(),error.getMessage(),Toast.LENGTH_LONG).show();
//            }
//        })
//        {
//            @Override
//            protected Map<String,String> getParams(){
//
//                Map<String , String> params = new HashMap<String, String>();
//
//                if(fcheck.equals("login2"))
//                {
//                    params.put("user_id",fuid);
//
//                }
//
//                else
//                {
//                    params.put("user_id",id);
//
//                }
//
//                return params;
//
//            }
//        };
//
//        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
//        requestQueue.add(stringRequest);
//    }


//    @Override
//    public void onItemClick(int position) {
//        Intent i = new Intent(getActivity(), Detail_user_view_event.class);
//        Event  clickedItem = eventListJoin.get(position);
//
//
//
//        i.putExtra("id",clickedItem.getId());
//        i.putExtra("image",clickedItem.getImage());
//        i.putExtra("name",clickedItem.getName());
//        i.putExtra("description",clickedItem.getDescription());
//        i.putExtra("date",clickedItem.getDate());
//        i.putExtra("dated",clickedItem.getDateddd());
//        i.putExtra("timed",clickedItem.getTimeddd());
//        i.putExtra("venue",clickedItem.getVenue());
//        i.putExtra("location",clickedItem.getLocation());
//
//
//        sharedPreferences = getActivity().getSharedPreferences("event", Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        editor.putString("name_event",clickedItem.getName());
//        editor.putString("date",clickedItem.getDate());
//        editor.putString("dated",clickedItem.getDateddd());
//        editor.putString("timed",clickedItem.getTimeddd());
//        editor.putString("event_id",clickedItem.getId());
//        editor.putString("location",clickedItem.getLocation());
//        editor.apply();
//
//        startActivity(i);
//
//    }
}
