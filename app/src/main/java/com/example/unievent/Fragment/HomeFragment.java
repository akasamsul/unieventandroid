package com.example.unievent.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.unievent.Adapter.MyAdapter_user_view_event;
import com.example.unievent.Details.Detail_user_home_NoComment;
import com.example.unievent.Model.Constants;
import com.example.unievent.Model.Event;
import com.example.unievent.R;
import com.example.unievent.Recommendation.Recommendation;
import com.example.unievent.UserActivity.NotificationViewActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomeFragment extends Fragment implements MyAdapter_user_view_event.OnItemClickListener {

    private static final String URL_DATA = Constants.BASE_URL+"/api/displayWithCategory";
    private RecyclerView recyclerView;

//    private RecyclerView.Adapter adapter;
    private MyAdapter_user_view_event myAdapter_user_view_event;

//        private  MyAdapterHomeNoComment myAdapterHomeNoComment;
    private List<Event> eventList;
    SharedPreferences sharedPreferences;

    String id,email,name,token;
    String fbid,fbeamail,fbname,fuid;
    String ucheck,fcheck;
//login2 = facebook,login3=user
//    View view;
    View rootView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        setHasOptionsMenu(true);
        rootView = inflater.inflate(R.layout.fragment_home,container,false);



        sharedPreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        id = sharedPreferences.getString("userID","");
        email = sharedPreferences.getString("userEmail","");
        name = sharedPreferences.getString("userName","");
        token = sharedPreferences.getString("userToken","");
        ucheck = sharedPreferences.getString("check","");
//
//
        sharedPreferences = getActivity().getSharedPreferences("userFacebook", Context.MODE_PRIVATE);
        fuid = sharedPreferences.getString("ID","");
        fbeamail = sharedPreferences.getString("fbEmail","");
        fbname = sharedPreferences.getString("fbName","");
        fbid = sharedPreferences.getString("fbID","");
        fcheck = sharedPreferences.getString("check","");







        recyclerView = rootView.findViewById(R.id.recyclerViewListEvent);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        eventList = new ArrayList<>();

        loadRecyclerViewData();

return rootView;

        }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {

        inflater.inflate(R.menu.notification_menu,menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId())
        {
            case R.id.menu_notification:
//                Toast.makeText(getContext(),"okay",Toast.LENGTH_LONG).show();
                Intent i = new Intent(getContext(), NotificationViewActivity.class);
                startActivity(i);
                break;

            case R.id.menu_Recommendation:
                Intent r = new Intent(getContext(), Recommendation.class);
                startActivity(r);
                break;
        }
        return super.onOptionsItemSelected(item);
    }



    private void loadRecyclerViewData(){
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("LOADING DATA...");
        progressDialog.show();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_DATA, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                progressDialog.dismiss();
                try {
                    JSONArray j = new JSONArray(response);

                    for (int i =0; i<j.length(); i++)
                    {
                        JSONObject obj= j.getJSONObject(i);



                        JSONArray obj2 = obj.getJSONArray("categories");
                        ArrayList category = new ArrayList();
                        String cate ="";
                        for(int k =0; k<obj2.length(); k++)
                        {
                            JSONObject objCategory = obj2.getJSONObject(k);
                            String catName =  objCategory.getString("category");


                            cate += catName+", ";


                        }
                        Event event = new Event(
                                obj.getString("id"),
                                obj.getString("name"),
                                obj.getString("description"),
                                obj.getString("date"),
                                obj.getString("venue"),
                                obj.getString("location"),
                                obj.getString("image"),
                                obj.getString("dated"),
                                obj.getString("timed"),
                                cate,
                                obj.getString("organizer")


                        );

                        Log.i("fdsafd",cate);
//                        event.getCategory()
                        eventList.add(event);

                    }

                    myAdapter_user_view_event = new MyAdapter_user_view_event(eventList,getContext());
                    recyclerView.setAdapter(myAdapter_user_view_event);
                    myAdapter_user_view_event.setOnItemClickListener(HomeFragment.this);


                } catch (Exception e) {
                    e.printStackTrace();
                }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(getContext(),error.getMessage(),Toast.LENGTH_LONG).show();
            }
        })
        {
            @Override
            protected Map<String,String> getParams(){

                Map<String , String> params = new HashMap<String, String>();

                if(fcheck.equals("login2"))
                {
                    params.put("user_id",fuid);

                }

                else
                {
                    params.put("user_id",id);

                }

                return params;

            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }



    @Override
    public void onItemClick(int position) {
        Intent i = new Intent(getActivity(), Detail_user_home_NoComment.class);
        Event  clickedItem = eventList.get(position);

//        sharedPreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
//        id = sharedPreferences.getString("userID","");
//        email = sharedPreferences.getString("userEmail","");
//        name = sharedPreferences.getString("userName","");
//        token = sharedPreferences.getString("userToken","");
//        event_id =sharedPreferences.getString("userToken","");


//        sharedPreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        editor.putString("userID",id);
//        editor.putString("userEmail",email);
//        editor.putString("userName",name);
//        editor.putString("userToken",token);
////        editor.putString("user_event_id",String.valueOf(position));
//
//        editor.putBoolean("userLogin",true);
//        editor.apply();





        i.putExtra("id",clickedItem.getId());
        i.putExtra("image",clickedItem.getImage());
        i.putExtra("name",clickedItem.getName());
        i.putExtra("description",clickedItem.getDescription());
        i.putExtra("date",clickedItem.getDate());
        i.putExtra("dated",clickedItem.getDateddd());
        i.putExtra("timed",clickedItem.getTimeddd());
        i.putExtra("venue",clickedItem.getVenue());
        i.putExtra("location",clickedItem.getLocation());
        i.putExtra("organizer",clickedItem.getOrganizer());


        sharedPreferences = getActivity().getSharedPreferences("event", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("name_event",clickedItem.getName());
        editor.putString("date",clickedItem.getDate());
        editor.putString("dated",clickedItem.getDateddd());
        editor.putString("timed",clickedItem.getTimeddd());
        editor.putString("event_id",clickedItem.getId());
        editor.putString("location",clickedItem.getLocation());
        editor.putString("venue",clickedItem.getVenue());
        editor.putString("organizer",clickedItem.getOrganizer());
        editor.apply();

        startActivity(i);
    }
}
