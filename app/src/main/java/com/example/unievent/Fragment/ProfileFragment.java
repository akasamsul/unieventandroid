package com.example.unievent.Fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.example.unievent.MainActivity;
import com.example.unievent.R;
import com.example.unievent.SessionManagement;
import com.example.unievent.UserActivity.user_change_password;
import com.example.unievent.UserActivity.user_edit_profile;
import com.example.unievent.UserActivity.user_view_interest_category;
import com.example.unievent.recyclerPage;
import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileFragment extends Fragment {

    SessionManagement session;
    SharedPreferences sharedPreferences;
    TextView welcome,TextEmail;
    Button btn_logout,btn_like,t;
    Button interest;
    Button edit_profile;

    String id,first_name,last_name, email , imageURL,check;

    CircleImageView circleImageView;

    FirebaseAuth mFirebasAuth;

    View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile,container,false);

//        session = new SessionManagement(getContext().getApplicationContext());
//        session.checkLogin();

        TextEmail = view.findViewById(R.id.text_email);

        welcome = view.findViewById(R.id.textView5);
        edit_profile = view.findViewById(R.id.user_edit_profile);

        edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), user_edit_profile.class);
                startActivity(i);
            }
        });
//

//        sharedPreferences = getActivity().getSharedPreferences("admin", Context.MODE_PRIVATE);
//        id = sharedPreferences.getString("adminID","");
//        email = sharedPreferences.getString("adminEmail","");
//        token = sharedPreferences.getString("adminToken","");
//        name = sharedPreferences.getString("adminName","");
//
//        welcome.setText(name+" "+ email);
//
//        btn_announcement = view.findViewById(R.id.bt_announcement);
//        btn_event = view.findViewById(R.id.bt_event);
//        btn_view = view.findViewById(R.id.bt_view);
//
//
//        btn_event.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(getContext(),Admin_create_event_activity.class);
//                startActivity(i);
//
//            }
//        });
//
//
//        btn_announcement.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(getContext(),Admin_create_announcement_activity.class);
//                startActivity(i);
//            }
//        });
//
//
//        btn_view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(getContext(),Admin_view_event_activity.class);
//                startActivity(i);
//            }
//        });





        interest = view.findViewById(R.id.BTN_interest);

        interest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), user_view_interest_category.class);
                startActivity(i);
            }
        });















        mFirebasAuth = FirebaseAuth.getInstance();

//        AppEventsLogger.activateApp(getContext());
//        FacebookSdk.sdkInitialize(getContext());

        t= view.findViewById(R.id.t);


        t.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), user_change_password.class);
                startActivity(i);
            }
        });

        welcome=view.findViewById(R.id.Text_name);
        btn_logout = view.findViewById(R.id.logout);
        btn_like=view.findViewById(R.id.button2);
        circleImageView = view.findViewById(R.id.circleImageView);
//
        sharedPreferences = getActivity().getSharedPreferences("facebookDisplay", Context.MODE_PRIVATE);
        id = sharedPreferences.getString("facebookID","");
        email = sharedPreferences.getString("facebookEmail","");
        first_name = sharedPreferences.getString("facebookName","");
        imageURL = sharedPreferences.getString("facebookImage","");
        String checkF = sharedPreferences.getString("check", "");



        sharedPreferences = getActivity().getSharedPreferences("user",Context.MODE_PRIVATE);
       String idU = sharedPreferences.getString("userID","");
       String emailU = sharedPreferences.getString("userEmail","");
       String first_nameU = sharedPreferences.getString("userName","");
//        imageURL = sharedPreferences.getString("userRole","");
       String  checkU = sharedPreferences.getString("check", "");


//        Bundle inBundle = getIntent().getExtras();
//        String name = inBundle.get("nama_dpn").toString();
//        String belakang = inBundle.get("nama_blkng").toString();
//        String id = inBundle.get("id").toString();

//        RequestOptions requestOptions = new RequestOptions();
//        requestOptions.dontAnimate();
//
        if (checkF.equals("login1"))
        {
            welcome.setText("WELCOME "+" "+first_name);
            TextEmail.setText(email);
//            t.setVisibility(view.INVISIBLE);
//            edit_profile.setVisibility(View.INVISIBLE);
            edit_profile.setEnabled(false);
            t.setEnabled(false);
        }
        else if(checkU.equals("login3"))
        {
            session = new SessionManagement(getContext().getApplicationContext());
            session.checkLogin();
            welcome.setText("WELCOME"+" "+first_nameU);
            TextEmail.setText(emailU);
        }



        Glide.with(getActivity()).load(imageURL).into(circleImageView);

        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                AlertDialog.Builder alt = new AlertDialog.Builder(getActivity());

                alt.setMessage("logout ?").setCancelable(false)
                        .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {


                                sharedPreferences = getActivity().getSharedPreferences("facebookDisplay", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
//                editor.putString("facebookID","");
//                editor.putString("facebookEmail","");
//                editor.putString("check","");
//                editor.putString("facebookName","");
//                editor.putString("facebookImage","");
//                editor.putString("userResponse","");
//                editor.putBoolean("facebookLogin",false);
//                editor.apply();
                                editor.clear();
                                editor.commit();
                                getActivity().finish();


                                sharedPreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor2 = sharedPreferences.edit();
                                editor2.putString("userID","");
                                editor2.putString("userEmail","");
                                editor2.putString("userName","");
                                editor2.putString("userRole","");
                                editor2.putString("userToken","");
                                editor2.putString("check","");
                                editor2.putBoolean("userLogin",false);
                                editor2.apply();

                                sharedPreferences = getActivity().getSharedPreferences("userFacebook", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor3 = sharedPreferences.edit();
                                editor3.putString("ID","");
                                editor3.putString("fbEmail","");
                                editor3.putString("fbName","");
                                editor3.putString("fbID","");
                                editor3.putString("fbRole","");
                                editor3.putString("check","");
                                editor3.putBoolean("fbLogin",false);
                                editor3.apply();




//                SharedPreferences preferences =getActivity().getSharedPreferences("facebookDisplay",Context.MODE_PRIVATE);
//                SharedPreferences.Editor editor = preferences.edit();
//                editor.clear();
//                editor.commit();
//                finish();


                                mFirebasAuth.signOut();
                                LoginManager.getInstance().logOut();

                                if (checkU.equals("login3"))
                                {
                                    session.logoutUser();
                                }


                                Intent login = new Intent(getContext(), MainActivity.class);
                                startActivity(login);
//                updateUI();
                                getActivity().finish();







                            }
                        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog alertDialog = alt.create();
                alertDialog.setTitle("LOGOUT");
                alertDialog.show();




            }
        });


        btn_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent likes = new Intent(getContext(), recyclerPage.class);
                startActivity(likes);
            }
        });


        return view;
    }


//    @Override
//    public void onStart() {
//        super.onStart();
//        // Check if user is signed in (non-null) and update UI accordingly.
//        FirebaseUser currentUser = mFirebasAuth.getCurrentUser();
//
//        if (currentUser == null) {
//
//            updateUI();
//
//        }
//    }
//
//    private void updateUI() {
//        Toast.makeText(getContext(),"logout",Toast.LENGTH_LONG).show();
//        Intent main = new Intent(getContext(),MainActivity.class);
//        startActivity(main);
////        finish();
//    }
}
