package com.example.unievent.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.unievent.Adapter.MyAdapterSearchEvent;
import com.example.unievent.Details.Detail_user_home_NoComment;
import com.example.unievent.Model.Constants;
import com.example.unievent.Model.Event;
import com.example.unievent.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

//import android.widget.SearchView;

public class SearchFragment extends Fragment implements MyAdapterSearchEvent.OnItemClickListener {

    private static final String URL_DATA = Constants.BASE_URL+"/api/getEvent";

    private RecyclerView recyclerView;
    private MyAdapterSearchEvent myAdapterSearchEvent;
    private List<Event> eventListSearch;

    SharedPreferences sharedPreferences;

    String id,email,name,token;
    String fbid,fbeamail,fbname,fuid;
    String ucheck,fcheck;

    View rootView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        rootView = inflater.inflate(R.layout.fragment_search,container,false);


        sharedPreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        id = sharedPreferences.getString("userID","");
//        email = sharedPreferences.getString("userEmail","");
//        name = sharedPreferences.getString("userName","");
//        token = sharedPreferences.getString("userToken","");
        ucheck = sharedPreferences.getString("check","");
//
//
        sharedPreferences = getActivity().getSharedPreferences("userFacebook", Context.MODE_PRIVATE);
        fuid = sharedPreferences.getString("ID","");
//        fbeamail = sharedPreferences.getString("fbEmail","");
//        fbname = sharedPreferences.getString("fbName","");
//        fbid = sharedPreferences.getString("fbID","");
        fcheck = sharedPreferences.getString("check","");




        recyclerView = rootView.findViewById(R.id.recyclerViewListEventSearch);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        eventListSearch = new ArrayList<>();
        loadRecyclerViewDataForSearch();

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.search_menu,menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (myAdapterSearchEvent !=null)
                {
                    myAdapterSearchEvent.getFilter().filter(newText);
                }
                return false;
            }
        });


        super.onCreateOptionsMenu(menu, inflater);
    }

    private void loadRecyclerViewDataForSearch(){
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("LOADING DATA...");
        progressDialog.show();


        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_DATA, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                progressDialog.dismiss();
                try {
                    JSONArray j = new JSONArray(response);

                    for (int i =0; i<j.length(); i++)
                    {
                        JSONObject obj= j.getJSONObject(i);


                        JSONArray obj2 = obj.getJSONArray("categories");
                        ArrayList category = new ArrayList();
                        String cate ="";
                        for(int k =0; k<obj2.length(); k++)
                        {
                            JSONObject objCategory = obj2.getJSONObject(k);
                            String catName =  objCategory.getString("category");


                            cate += catName+", ";


                        }
                        Event event = new Event(
                                obj.getString("id"),
                                obj.getString("name"),
                                obj.getString("description"),
                                obj.getString("date"),
                                obj.getString("venue"),
                                obj.getString("location"),
                                obj.getString("image"),
                                obj.getString("dated"),
                                obj.getString("timed"),
                                cate,
                                obj.getString("organizer")


                        );

                        Log.i("fdsafd",cate);
//                        event.getCategory()
                        eventListSearch.add(event);

                    }

                    myAdapterSearchEvent = new MyAdapterSearchEvent(eventListSearch,getContext());
                    recyclerView.setAdapter(myAdapterSearchEvent);
                    myAdapterSearchEvent.setOnClickListener(SearchFragment.this);


                } catch (Exception e) {
                    e.printStackTrace();
                }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(getContext(),error.getMessage(),Toast.LENGTH_LONG).show();
            }
        });


        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }

    @Override
    public void onItemClick(int position) {
        Intent i = new Intent(getActivity(), Detail_user_home_NoComment.class);
        Event  clickedItem = eventListSearch.get(position);



        i.putExtra("id",clickedItem.getId());
        i.putExtra("image",clickedItem.getImage());
        i.putExtra("name",clickedItem.getName());
        i.putExtra("description",clickedItem.getDescription());
        i.putExtra("date",clickedItem.getDate());
        i.putExtra("dated",clickedItem.getDateddd());
        i.putExtra("timed",clickedItem.getTimeddd());
        i.putExtra("venue",clickedItem.getVenue());
        i.putExtra("location",clickedItem.getLocation());
        i.putExtra("organizer",clickedItem.getOrganizer());


        sharedPreferences = getActivity().getSharedPreferences("event", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("name_event",clickedItem.getName());
        editor.putString("date",clickedItem.getDate());
        editor.putString("dated",clickedItem.getDateddd());
        editor.putString("timed",clickedItem.getTimeddd());
        editor.putString("event_id",clickedItem.getId());
        editor.putString("location",clickedItem.getLocation());
        editor.putString("venue",clickedItem.getVenue());
        editor.putString("organizer",clickedItem.getOrganizer());
        editor.apply();

        startActivity(i);
    }
}
