package com.example.unievent.Fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.unievent.Adapter.MyAdapterEvent;
import com.example.unievent.AdminActivity.DetailActivity_event_admin;
import com.example.unievent.Model.Constants;
import com.example.unievent.Model.Event;
import com.example.unievent.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabUpComingEventAdmin extends Fragment implements MyAdapterEvent.OnItemClickListener{


    public TabUpComingEventAdmin() {
        // Required empty public constructor
    }


    private RequestQueue mQueue;

    SharedPreferences sharedPreferences;
    String id,email,token,Ename;


    private static final String URL_DATA = Constants.BASE_URL+"/api/displayAdminEvent";
    private RecyclerView recyclerView;



    private MyAdapterEvent myAdapterEvent;

    private List<Event> eventList;

    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView= inflater.inflate(R.layout.fragment_tab_up_coming_event_admin, container, false);

        recyclerView = rootView.findViewById(R.id.recyclerViewListEvent);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        eventList = new ArrayList<>();


        sharedPreferences = getContext().getSharedPreferences("admin", Context.MODE_PRIVATE);
        id = sharedPreferences.getString("adminID","");
        email = sharedPreferences.getString("adminEmail","");
        token = sharedPreferences.getString("adminToken","");
        Ename = sharedPreferences.getString("adminName","");


//        adapter = new MyAdapterEvent(eventList,this);
//        recyclerView.setAdapter(adapter);




        loadRecyclerViewData();





//
//
//        mQueue = Volley.newRequestQueue(this);


//        jsonParse();


        return rootView;
    }


    private void loadRecyclerViewData(){
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("LOADING DATA...");
        progressDialog.show();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_DATA, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                progressDialog.dismiss();
                try {
                    JSONArray j = new JSONArray(response);

                    for (int i =0; i<j.length(); i++)
                    {
                        JSONObject obj= j.getJSONObject(i);

                        JSONArray obj2 = obj.getJSONArray("categories");
                        ArrayList category = new ArrayList();
                        String cate ="";
                        for(int k =0; k<obj2.length(); k++)
                        {
                            JSONObject objCategory = obj2.getJSONObject(k);
                            String catName =  objCategory.getString("category");


                            cate += catName+", ";


                        }
                        Event event = new Event(
                                obj.getString("id"),
                                obj.getString("name"),
                                obj.getString("description"),
                                obj.getString("date"),
                                obj.getString("venue"),
                                obj.getString("location"),
                                obj.getString("image"),
                                obj.getString("dated"),
                                obj.getString("timed"),
                                cate,
                                obj.getString("comment"),
                                obj.getString("organizer")


                        );

                        Log.i("fdsafd",cate);
                        eventList.add(event);

                    }

                    myAdapterEvent = new MyAdapterEvent(eventList,getContext());
                    recyclerView.setAdapter(myAdapterEvent);
                    myAdapterEvent.setOnItemClickListener(TabUpComingEventAdmin.this);


                } catch (Exception e) {
                    e.printStackTrace();
                }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(getContext(),error.getMessage(),Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String,String> getParams(){

                Map<String , String> params = new HashMap<String, String>();

                params.put("user_id",id);

                return params;

            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }

    @Override
    public void onItemClick(int position) {
        Intent i = new Intent(getActivity(), DetailActivity_event_admin.class);
        Event  clickedItem = eventList.get(position);

        i.putExtra("id",clickedItem.getId());
        i.putExtra("image",clickedItem.getImage());
        i.putExtra("name",clickedItem.getName());
        i.putExtra("description",clickedItem.getDescription());
        i.putExtra("date",clickedItem.getDate());
        i.putExtra("dated",clickedItem.getDateddd());
        i.putExtra("timed",clickedItem.getTimeddd());
        i.putExtra("venue",clickedItem.getVenue());
        i.putExtra("location",clickedItem.getLocation());
        i.putExtra("comment",clickedItem.getComment());
        i.putExtra("organizer",clickedItem.getOrganizer());
        i.putExtra("category",clickedItem.getCategory2());


        sharedPreferences = getContext().getSharedPreferences("event", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("event_id",clickedItem.getId());
        editor.putString("timed",clickedItem.getTimeddd());
        editor.apply();

        startActivity(i);
    }
}
