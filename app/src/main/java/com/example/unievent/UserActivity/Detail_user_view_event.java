package com.example.unievent.UserActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.unievent.Model.Constants;
import com.example.unievent.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Detail_user_view_event extends AppCompatActivity {

    Button btn_join, btn_register;

    SharedPreferences sharedPreferences;
    String id,email,name,token,user_event_id,ucheck;
    String comment,categoryType;
    String fid,fbid,femail,fname,fcheck;
    String mJoin ;
    final static String URL_DATA = Constants.BASE_URL+"/api/getUserEvent ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_user_view_event);


        sharedPreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        id = sharedPreferences.getString("userID","");
        email = sharedPreferences.getString("userEmail","");
        name = sharedPreferences.getString("userName","");
        token = sharedPreferences.getString("userToken","");
        ucheck = sharedPreferences.getString("check","");
//        user_event_id = sharedPreferences.getString("user_event_id","");


        sharedPreferences = getSharedPreferences("userFacebook", Context.MODE_PRIVATE);
        fid = sharedPreferences.getString("ID","");
        femail = sharedPreferences.getString("fbEmail","");
        fname = sharedPreferences.getString("fbName","");
        fcheck = sharedPreferences.getString("check","");


        btn_join = findViewById(R.id.btn_join);
        btn_register = findViewById(R.id.btn_register);

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Detail_user_view_event.this,RegisterEvent.class);
                startActivity(i);
            }
        });

        Intent i = getIntent();
        user_event_id = i.getStringExtra("id");
        String image = i.getStringExtra("image");
        String title = i.getStringExtra("name");
        String description = i.getStringExtra("description");
        String date = i.getStringExtra("date");
        String location = i.getStringExtra("location");
        String venue = i.getStringExtra("venue");
        comment = i.getStringExtra("comment");


        ImageView imageView = findViewById(R.id.image_detail_user);

        TextView textViewT = findViewById(R.id.title_detail_user);
        TextView textViewD = findViewById(R.id.description_detail_user);
        TextView textViewDate = findViewById(R.id.date_detail_user);
        TextView textViewVenue = findViewById(R.id.venue_detail_user);
//        TextView textViewLocation =findViewById(R.id.location_detail_user);
        TextView tComment = findViewById(R.id.comment);



//        Picasso.get().load(image).fit().centerInside().into(imageView);
        Picasso.get().load(image).into(imageView);

        DateFormat outputFormat = new SimpleDateFormat("EEE, dd/MM/yyyy hh.mm aa");
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date1 = null;
        try {
            date1 = inputFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outputText = outputFormat.format(date1);


        textViewT.setText(title);
        textViewD.setText(description);

        textViewDate.setText(outputText);
        textViewVenue.setText(venue);
//        textViewLocation.setText(venue);
        tComment.setText(comment);

        isJoinedEvent();
//        setDisabledButoon();


//        if()
//        {
//            btn_join.setEnabled(false);
//        }
//
//        else
//        btn_join.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                postJoinEvent();
//
//
//            }
//        });

    }

//    private void setDisabledButoon() {
//
//        if (mJoin.equals("true"))
//        {
//            btn_join.setEnabled(false);
//        }
//        else{
//
//            btn_join.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    postJoinEvent();
//
//
//                }
//            });
//
//        }
//    }

    private void isJoinedEvent() {

        String BASE_URL = Constants.BASE_URL+"/api/CheckUserEvent";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                Toast.makeText(Detail_user_view_event.this,response.toString(),Toast.LENGTH_LONG).show();

                try {
                    JSONObject jresponse = new JSONObject(response);
                     mJoin = jresponse.getString("result");
                     Log.i("masuk",mJoin.toString());

                    if (mJoin.equals("true"))
                    {
                        btn_join.setEnabled(false);
                        btn_register.setVisibility(View.INVISIBLE);
                        btn_register.setEnabled(false);
                    }
                    else{

                        btn_join.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                postJoinEvent();


                            }
                        });

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Detail_user_view_event.this,error.getMessage(),Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            protected Map<String,String> getParams(){

                Map<String , String> params = new HashMap<String, String>();

                if(fcheck.equals("login2"))
                {
                    params.put("user_id",fid);
                    params.put("event_id",user_event_id);
                }

                else
                {
                    params.put("user_id",id);
                    params.put("event_id",user_event_id);
                }





                Log.i("masukla",params.toString());

                return params;

            }
        };
        requestQueue.add(stringRequest);

    }


    private void postJoinEvent()
    {

        String BASE_URL = Constants.BASE_URL+"/api/userRegisterEvent";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(Detail_user_view_event.this,response.toString(),Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Detail_user_view_event.this,error.getMessage(),Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            protected Map<String,String> getParams(){

                Map<String , String> params = new HashMap<String, String>();

                if(fcheck.equals("login2"))
                {
                    params.put("user_id",fid);
                    params.put("event_id",user_event_id);
                }

                else
                {
                    params.put("user_id",id);
                    params.put("event_id",user_event_id);
                }

//                params.put("user_id",id);
//                params.put("event_id",user_event_id);
//                params.put("status","success");


                Log.i("masukla",params.toString());

                return params;

            }
        };
        requestQueue.add(stringRequest);

    }
}
