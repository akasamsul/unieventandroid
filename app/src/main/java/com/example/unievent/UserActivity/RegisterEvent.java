package com.example.unievent.UserActivity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.unievent.Model.Constants;
import com.example.unievent.Notification.AlarmReceiver;
import com.example.unievent.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

public class RegisterEvent extends AppCompatActivity {

    Button submit;

    Switch notification_status;
    EditText Mname,Mphone,Mmatrix,Mcourse;
    String user_event_id,id;
    String Mstatus = "off" ;
    SharedPreferences sharedPreferences;

    String fid,femail,fname,fcheck;
    String emailUser,nameUser;

    String Vname,Vmatrix,Vprogram,Vphone;

    String nameEvent,dateEvent,mdated,mtimed,location,venue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_event);

        sharedPreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        id = sharedPreferences.getString("userID","");
        emailUser = sharedPreferences.getString("userEmail","");
        nameUser = sharedPreferences.getString("userName","");


        sharedPreferences = getSharedPreferences("userFacebook", Context.MODE_PRIVATE);
        fid = sharedPreferences.getString("ID","");
        femail = sharedPreferences.getString("fbEmail","");
        fname = sharedPreferences.getString("fbName","");
        fcheck = sharedPreferences.getString("check","");

        sharedPreferences = getSharedPreferences("event", Context.MODE_PRIVATE);
        user_event_id = sharedPreferences.getString("event_id","");
        nameEvent = sharedPreferences.getString("name_event","");
        dateEvent = sharedPreferences.getString("date","");
        mdated = sharedPreferences.getString("dated","");
        mtimed = sharedPreferences.getString("timed","");
        location = sharedPreferences.getString("location","");
        venue=sharedPreferences.getString("venue","");

//        Intent i = getIntent();
//        user_event_id = i.getStringExtra("id");
//        String image = i.getStringExtra("image");
//        String title = i.getStringExtra("name");
//        String description = i.getStringExtra("description");
//        String date = i.getStringExtra("date");
//        String location = i.getStringExtra("location");
//        String venue = i.getStringExtra("venue");

        Mname = findViewById(R.id.nameUser);
        Mmatrix = findViewById(R.id.matrixNO);
        Mcourse = findViewById(R.id.courseUser);
        Mphone = findViewById(R.id.phoneUser);

        Mname.setText(nameUser);

        submit = findViewById(R.id.submitReq);
        notification_status = findViewById(R.id.switch1);

        notification_status.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked == true)
                {
                    Mstatus = "on";

                }
                else
                {
                    Mstatus = "off";
                }
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                check();

//                Register();
//                if (Mstatus == "on")
//                {
//                    notification();
//                }

            }
        });


    }

    private void check()
    {
        intialize();

        if(!valitation())
        {

            Toast.makeText(RegisterEvent.this,"Register Fail, please try again",Toast.LENGTH_LONG).show();

        }

        else
        {
            Register();
                if (Mstatus == "on")
                {
                    notification();
                }


        }
    }

    private void intialize()
    {
        Vname = Mname.getText().toString();
        Vmatrix = Mmatrix.getText().toString();
        Vprogram = Mcourse.getText().toString();
        Vphone = Mphone.getText().toString();
    }


    private boolean valitation()
    {
        boolean valid = true;
        if(Vname.isEmpty())
        {
//            !e.matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+")
            Mname.setError("enter a name");
            valid = false;
        }


        if(Vmatrix.isEmpty()  || Vmatrix.length() <10)
        {
//            password.setError("enter valid password");
//            valid = false;



            if(Vmatrix.length() < 10)
            {
                Mmatrix.setError("enter a valid matrix number ");
                valid = false;
            }

        }

        if(Vprogram.isEmpty() ||  Vprogram.length() > 5 )
        {
            Mcourse.setError("enter valid program code");
            valid = false;
        }


        if(Vphone.isEmpty())
        {
            Mphone.setError("please enter number phone");
            valid = false;
        }

        return valid;
    }


    private void notification()
    {


// converting string to date , and to get yesterday date.
//        DateFormat df = new SimpleDateFormat("yyyy-M-dd hh:mm:ss");
//        GregorianCalendar gc = new GregorianCalendar();
//
//        Date date = null;
//        try {
//            date = df.parse(dateEvent);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        Calendar cal = Calendar.getInstance();
//         cal.setTime(date);
//
//        gc.setTime(date);
//        int dayBefore = gc.get(Calendar.DAY_OF_YEAR);
//        gc.roll(Calendar.DAY_OF_YEAR, -1);
//        int dayAfter = gc.get(Calendar.DAY_OF_YEAR);
//        if(dayAfter > dayBefore) {
//            gc.roll(Calendar.YEAR, -1);
//        }
//        gc.get(Calendar.DATE);
//        java.util.Date yesterday = gc.getTime();
//        System.out.println(yesterday);
//
//        NotifyMe notifyMe = new NotifyMe.Builder(getApplicationContext())
//                .title("Don't Forget Your Event Tomorrow ! ")
//                .content(nameEvent +" " + "on" + " " + mtimed + " "+ "at" + " " +location )
//                .color(255,0,0,255)
//                .led_color(255,255,255,255)
//                .time(yesterday)
//                .addAction(new Intent(), "snooze",false)
//                .key("test")
//                .addAction(new Intent(),"Dismiss", true,false)
//                .addAction(new Intent(),"Done")
//                .large_icon(R.mipmap.ic_launcher_round)
//
//                .build();
//
//
//        Log.i("okay",dateEvent);
//        Log.i("MASUK", String.valueOf(date));







        /// new notification with channel

        sharedPreferences = getSharedPreferences("Notification", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("event_id",user_event_id);
        editor.putString("name_event",nameEvent);
        editor.putString("date",dateEvent);
        editor.putString("dated",mdated);
        editor.putString("timed",mtimed);
        editor.putString("venue",venue);
        editor.putString("location",location);
        editor.apply();




        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        Intent notificationIntent = new Intent(this, AlarmReceiver.class);
        PendingIntent broadcast = PendingIntent.getBroadcast(this, 100, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);


        DateFormat df = new SimpleDateFormat("yyyy-M-dd hh:mm:ss");
        GregorianCalendar gc = new GregorianCalendar();

        Date date = null;
        try {
            date = df.parse(dateEvent);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cala = Calendar.getInstance();
         cala.setTime(date);

        gc.setTime(date);
        int dayBefore = gc.get(Calendar.DAY_OF_YEAR);
        gc.roll(Calendar.DAY_OF_YEAR, -1);
        int dayAfter = gc.get(Calendar.DAY_OF_YEAR);
        if(dayAfter > dayBefore) {
            gc.roll(Calendar.YEAR, -1);
        }
        gc.get(Calendar.DATE);
        java.util.Date yesterday = gc.getTime();
        System.out.println(yesterday);
        Calendar cal = Calendar.getInstance();
        cal.setTime(yesterday);






//        DateFormat df = new SimpleDateFormat("yyyy-M-dd hh:mm:ss");
//        GregorianCalendar gc = new GregorianCalendar();
//
//        Date date = null;
//        try {
//            date = df.parse("2019-12-25 15:22:00");
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//        Calendar cal = Calendar.getInstance();
//        cal.setTime(date);

        alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), broadcast);







    }

    private void Register()
    {
        String BASE_URL = Constants.BASE_URL+"/api/userRegisterEventWithDetails";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(RegisterEvent.this,"Congratulation your have register to the event",Toast.LENGTH_LONG).show();


                Intent i = new Intent(RegisterEvent.this,UserProfileActivity.class);
                startActivity(i);
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(RegisterEvent.this,error.getMessage(),Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            protected Map<String,String> getParams(){



                Map<String , String> params = new HashMap<String, String>();

                if(fcheck.equals("login2"))
                {
                    params.put("user_id",fid);

                }
                else
                {
                    params.put("user_id",id);
                }



                params.put("event_id",user_event_id);

                params.put("name",Mname.getText().toString());
                params.put("matrix",Mmatrix.getText().toString());
                params.put("nophone",Mphone.getText().toString());

                params.put("course",Mcourse.getText().toString());
                params.put("status",Mstatus);

                Log.i("masukla",params.toString());
                return params;

            }
        };
        requestQueue.add(stringRequest);

    }
}
