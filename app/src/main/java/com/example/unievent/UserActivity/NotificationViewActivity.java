package com.example.unievent.UserActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.unievent.Adapter.MyAdapterList;
import com.example.unievent.Model.Announcement;
import com.example.unievent.Model.Constants;
import com.example.unievent.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NotificationViewActivity extends AppCompatActivity {

    RecyclerView recyclerView;

    MyAdapterList myAdapterList;

    List<Announcement> announcementList;
    SharedPreferences sharedPreferences;
    String fuid,fbeamail,fbname,fbid,checkF;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_view);


        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        announcementList = new ArrayList<>();

        checkUser();


    }

    private void checkUser()
    {
        sharedPreferences = getSharedPreferences("userFacebook", Context.MODE_PRIVATE);
        fuid = sharedPreferences.getString("ID","");
        fbeamail = sharedPreferences.getString("fbEmail","");
        fbname = sharedPreferences.getString("fbName","");
        fbid = sharedPreferences.getString("fbID","");
        checkF = sharedPreferences.getString("check","");


        loadRecyclerViewData();

//        if (checkF.equals("login2"))
//        {
//            loadRecyclerViewData();
//        }
//        else
//        {
//            Toast.makeText(NotificationViewActivity.this,"no data yet",Toast.LENGTH_LONG).show();
//        }
    }


    private void loadRecyclerViewData(){
        String URL_DATA = Constants.BASE_URL+"/api/getAnnouncements";
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("LOADING DATA...");
        progressDialog.show();


        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_DATA, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                progressDialog.dismiss();
                try {
                    JSONArray j = new JSONArray(response);

                    for (int i =0; i<j.length(); i++)
                    {
                        JSONObject obj= j.getJSONObject(i);

                        Announcement announcement = new Announcement(
                                obj.getString("title"),
                                obj.getString("description")

                        );
                        announcementList.add(announcement);

                    }

                    myAdapterList = new MyAdapterList(announcementList,getApplicationContext());
                    recyclerView.setAdapter(myAdapterList);


                } catch (Exception e) {
                    e.printStackTrace();
                }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(NotificationViewActivity.this,error.getMessage(),Toast.LENGTH_LONG).show();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


}
