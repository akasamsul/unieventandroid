package com.example.unievent.UserActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.unievent.R;

import java.util.ArrayList;
import java.util.List;

public class newUser_pickCategory_activity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    CheckBox cb1,cb2,cb3;

    TextView textView;

    Button btn_send;

    RadioGroup radioGroup,radioGroup2;
    RadioButton radioButton,radioButton2;

    RadioButton one, two ,three;

    TextView test;

    String display,text,tempRemovedItem;
    Spinner spinner,spinner2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_user_pick_category_activity);





         spinner = findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.category,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);


         spinner2 = findViewById(R.id.spinner2);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,R.array.category,android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter);
        spinner2.setOnItemSelectedListener(this);

//        cb1 = findViewById(R.id.outdoor);
//        cb2 = findViewById(R.id.other);
//        cb3 = findViewById(R.id.food);

//        adapter2.no

        one = findViewById(R.id.one);
        two = findViewById(R.id.two);
        three = findViewById(R.id.three);

        textView = findViewById(R.id.view);

        btn_send = findViewById(R.id.send);

        radioGroup = findViewById(R.id.mostLike);

        radioGroup2 = findViewById(R.id.secLike);

        int radioId = radioGroup.getCheckedRadioButtonId();
        radioButton = findViewById(radioId);
//        String text = radioButton.getText().toString();
//        textView.setText(radioButton.getText());


//        int radioid2 = radioGroup2.getCheckedRadioButtonId();
//        radioButton2 = findViewById(radioid2);
        test = findViewById(R.id.test);


        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                int radioId = radioGroup.getCheckedRadioButtonId();
//                radioButton = findViewById(radioId);
//
//                textView.setText(radioButton.getText());
                test.setText(text+"\n"+display);
            }
        });

    }

    public void o(View view)
    {
       boolean checked = ((RadioButton) view).isChecked();
        String str="";
        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.one:
                if(checked)
                    two.setEnabled(false);
                str = "Android Selected";
                break;
            case R.id.two:
                if(checked)
                str = "AngularJS Selected";
                break;
            case R.id.three:
                if(checked)
                str = "Java Selected";
                break;

        }
        Toast.makeText(getApplicationContext(), str, Toast.LENGTH_SHORT).show();




    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//        switch (parent.getId())
//        {
//            case R.id.spinner:
//                text = parent.getItemAtPosition(position).toString();
//                break;
//            case R.id.spinner2:
//                display = parent.getItemAtPosition(position).toString();
//                break;
//
//        }

//        data2.remove(spinner1.getSelectedItem().toString());
//
//        if(tempRemovedItem != null){
//            data2.add(tempRemovedItem);
//        }
//
//        tempRemovedItem = spinner1.getSelectedItem().toString();
//        adapter2.notifyDataSetChanged();

//        text = parent.getItemAtPosition(position).toString();
//        display = parent.getItemAtPosition(2).toString();
//        Toast.makeText(parent.getContext(),text,Toast.LENGTH_LONG).show();


        List<String> data = new ArrayList<>(R.array.category);
        List<String> data2 = new ArrayList<>(R.array.category);

        switch (parent.getId())
        {
            case R.id.spinner:

                data2.remove(parent.getSelectedItem().toString());
                if (tempRemovedItem!=null)
                {
                    data2.add(tempRemovedItem);
                }

                tempRemovedItem =parent.getSelectedItem().toString();
//                spinner2.notifyDataSetChanged();

                text = parent.getItemAtPosition(position).toString();
                break;
            case R.id.spinner2:
                display = parent.getItemAtPosition(position).toString();
                break;

        }

    }








    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}

//    public void checkone(View v)
//    {
//        if(cb1.isChecked() && cb2.isChecked() && cb3.isChecked())
//        {
//            textView.setText("outdoor and other and food");
//        }
//
//
//
//       else if(cb1.isChecked() && cb2.isChecked())
//        {
//            textView.setText("outdoor and other");
//        }
//
//       else if(cb2.isChecked() && cb3.isChecked())
//        {
//            textView.setText("food and other");
//        }
//
//        else if(cb1.isChecked() && cb3.isChecked())
//        {
//            textView.setText("food and outdoor");
//        }
//
//        else if (cb1.isChecked())
//        {
//            textView.setText("outdoor");
//        }
//
//        else if (cb2.isChecked())
//        {
//            textView.setText("other");
//        }
//
//        else if (cb3.isChecked())
//        {
//            textView.setText("food");
//        }
//    }






