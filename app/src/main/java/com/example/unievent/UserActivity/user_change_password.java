package com.example.unievent.UserActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.unievent.Model.Constants;
import com.example.unievent.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class user_change_password extends AppCompatActivity {


    SharedPreferences sharedPreferences;
    String id,name,email,token;
    EditText current,newPassword,confirmPassword;

    Button submit;

    String c,np,cp;

    String idU,emailU, first_nameU, checkU;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_change_password);


        sharedPreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
         idU = sharedPreferences.getString("userID","");
         emailU = sharedPreferences.getString("userEmail","");
       first_nameU = sharedPreferences.getString("userName","");
//        imageURL = sharedPreferences.getString("userRole","");
         checkU = sharedPreferences.getString("check", "");

        current = findViewById(R.id.old_password);
        newPassword = findViewById(R.id.new_password);
        confirmPassword = findViewById(R.id.confirm_password);

        submit = findViewById(R.id.change_password);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                change();
            }
        });


    }


    private void change()
    {
        intialize();

        if(!valitation())
        {

            Toast.makeText(user_change_password.this,"change password fail",Toast.LENGTH_LONG).show();

        }

        else
        {
            changePassword();

        }
    }

    private void intialize()
    {
        c = current.getText().toString();
        np = newPassword.getText().toString();
        cp = confirmPassword.getText().toString();

    }


    private boolean valitation()
    {
        boolean valid = true;
        if(c.isEmpty())
        {
//            !e.matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+")
            current.setError("enter valid email");
            valid = false;
        }


        if(np.isEmpty() || !np.equals(cp) || np.length() <8)
        {
//            password.setError("enter valid password");
//            valid = false;

            if(!np.equals(cp))
            {
                newPassword.setError("confirm password does not same");
                valid = false;



            }

            if(np.length() < 8)
            {
                newPassword.setError("minimum character is 8 ");
                valid = false;
            }

        }

        if(cp.isEmpty() || !cp.equals(np) || cp.length() < 8)
        {
            confirmPassword.setError("enter valid password");
            valid = false;
        }



        return valid;
    }


    String result;
    private void changePassword()
    {

        String BASE_URL = Constants.BASE_URL+"/api/updatePassword";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(user_change_password.this,response.toString(),Toast.LENGTH_LONG).show();
//

                try {
                    JSONObject jresponse = new JSONObject(response);
                    result = jresponse.getString("result");


                    if (result.equals("success"))
                    {
//                        Toast.makeText(admin_change_password.this,"success",Toast.LENGTH_LONG).show();
                        Intent i = new Intent(user_change_password.this, UserProfileActivity.class);
                        startActivity(i);
                    }
//                    else
//                    {
//                        Toast.makeText(admin_change_password.this,response.toString(),Toast.LENGTH_LONG).show();
//                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(user_change_password.this,error.getMessage(),Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            protected Map<String,String> getParams(){

                Map<String , String> params = new HashMap<String, String>();

                params.put("email",emailU);
                params.put("password",current.getText().toString());
                params.put("new_password",newPassword.getText().toString());

                Log.i("masukla",params.toString());

                return params;

            }
        };
        requestQueue.add(stringRequest);




    }



}
