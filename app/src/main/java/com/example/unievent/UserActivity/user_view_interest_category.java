package com.example.unievent.UserActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.unievent.Adapter.MyAdapter_interest_category_user;
import com.example.unievent.Model.Category;
import com.example.unievent.Model.Constants;
import com.example.unievent.R;
import com.example.unievent.UserRegisterCategory;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class user_view_interest_category extends AppCompatActivity {

  public static   Button update_interest;

    private RecyclerView recyclerView;

    private static final String URL_DATA = Constants.BASE_URL+"/api/categoryInterestUser";

    SharedPreferences sharedPreferences;
    String fid,femail,fname,fcheck;
    String user_event_id,id;
    String nameEvent,dateEvent,mdated,mtimed,location;

    private MyAdapter_interest_category_user myAdapter_interest_category_user;
    private List<Category> categoryList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_view_interest_category);


        sharedPreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        id = sharedPreferences.getString("userID","");


        sharedPreferences = getSharedPreferences("userFacebook", Context.MODE_PRIVATE);
        fid = sharedPreferences.getString("ID","");
        femail = sharedPreferences.getString("fbEmail","");
        fname = sharedPreferences.getString("fbName","");
        fcheck = sharedPreferences.getString("check","");

        sharedPreferences = getSharedPreferences("event", Context.MODE_PRIVATE);
        user_event_id = sharedPreferences.getString("event_id","");
        nameEvent = sharedPreferences.getString("name_event","");
        dateEvent = sharedPreferences.getString("date","");
        mdated = sharedPreferences.getString("dated","");
        mtimed = sharedPreferences.getString("timed","");
        location = sharedPreferences.getString("location","");


        update_interest = findViewById(R.id.update_interest);
                update_interest.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder alt= new AlertDialog.Builder(user_view_interest_category.this);

                        alt.setMessage("Do you want to change your preferences ?, this will reset your preferences").setCancelable(false)
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
//                                        Intent i = new Intent(user_view_interest_category.this, MainActivity.class);
//                                        startActivity(i);

//                                        Toast.makeText(user_view_interest_category.this,id.toString(),Toast.LENGTH_LONG).show();


                                        deleteCategory();

                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert = alt.create();
                        alert.setTitle("RESET PREFERENCES");
                        alert.show();
                    }
                });






        recyclerView = findViewById(R.id.recyclerViewInterest);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        categoryList = new ArrayList<>();


        loadRecyclerViewDataInterest();

    }



    private void loadRecyclerViewDataInterest(){
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("LOADING DATA...");
        progressDialog.show();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_DATA, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                progressDialog.dismiss();
                try {
                    JSONArray j = new JSONArray(response);

                    for (int i =0; i<j.length(); i++)
                    {
                        JSONObject obj= j.getJSONObject(i);

                        Category cat = new Category(
                                obj.getString("category")



                        );
                        categoryList.add(cat);

                    }

                    myAdapter_interest_category_user = new MyAdapter_interest_category_user(categoryList,getApplicationContext());
                    recyclerView.setAdapter(myAdapter_interest_category_user);
//                    adapter.setOnItemClickListener(Admin_view_announcement_activity.this);

                } catch (Exception e) {
                    e.printStackTrace();
                }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
//                Toast.makeText(Admin_view_announcement_activity.this,error.getMessage(),Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<String, String>();


                if(fcheck.equals("login2"))
                {
                    params.put("user_id",fid);

                }
                else
                {
                    params.put("user_id",id);
                }

                return params;

            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    public void deleteCategory()
    {
        String deteleUrl;

        if(fcheck.equals("login2"))
        {

            deteleUrl =  Constants.BASE_URL+"/api/updateProfilingByDelete/"+fid;

        }

        else
        {
             deteleUrl =  Constants.BASE_URL+"/api/updateProfilingByDelete/"+id;

        }
//        String deteleUrl =  Constants.BASE_URL+"/api/updateProfilingByDelete/"+id;
//        String BASE_URL = Constants.BASE_URL+"/api/createEvent";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, deteleUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                Toast.makeText(Admin_create_event_activity.this,response.toString(),Toast.LENGTH_LONG).show();
                Intent i = new Intent(user_view_interest_category.this, UserRegisterCategory.class);
                startActivity(i);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(Admin_create_event_activity.this,error.getMessage(),Toast.LENGTH_LONG).show();

            }
        });

        requestQueue.add(stringRequest);
    }



//    public void dialogevent(View view)
//    {
//        update_interest = findViewById(R.id.update_interest);
//        update_interest.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                AlertDialog.Builder alt= new AlertDialog.Builder(user_view_interest_category.this);
//
//                alt.setMessage("Do want to change your interest ?, this will reset your interest").setCancelable(false)
//                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//
//                        }
//                    })
//                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.cancel();
//                            }
//                        });
//
//                AlertDialog alert = alt.create();
//                alert.setTitle("TEST");
//                alert.show();
//            }
//        });
//
//    }


}
