package com.example.unievent.UserActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.unievent.MainActivity;
import com.example.unievent.Model.Constants;
import com.example.unievent.Model.User;
import com.example.unievent.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class registerActivity extends AppCompatActivity {

    EditText email,password,c_password,name;

//    FirebaseAuth mFirebaseAuth;
//    FirebaseFirestore db;

    String e,p,c,n;
    String idR;
    String emailR;
    String nameR;
    String tokenR;
    String uRole;


    SharedPreferences sharedPreferences;

    Button btn_reg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

//        mFirebaseAuth = FirebaseAuth.getInstance();
//        db = FirebaseFirestore.getInstance();

        email = findViewById(R.id.email1);
        password = findViewById(R.id.password1);
        c_password = findViewById(R.id.c_password1);
        name = findViewById(R.id.name1);
        btn_reg = findViewById(R.id.reg);


//        e = email.getText().toString();
//        p = password.getText().toString();
//        c = c_password.getText().toString();
//        n = name.getText().toString();


        btn_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//
                register();
//                postRegisterNormalUser();

//
                    }
//
            });

    }

    private void register()
    {
        intialize();

        if(!valitation())
        {

            Toast.makeText(registerActivity.this,"Register Fail, please try again",Toast.LENGTH_LONG).show();

        }

        else
        {
            signUp();

//            if(duplicate.equals("exists"))
//            {
//                Toast.makeText(registerActivity.this,"Account already register !",Toast.LENGTH_LONG).show();
//            }
//            else
//            {
//
//                Toast.makeText(registerActivity.this,"Congratulation Register Success, Please Login Again",Toast.LENGTH_LONG).show();
//                Intent i = new Intent(registerActivity.this, MainActivity.class);
//                startActivity(i);
//            }

        }
    }

    private void intialize()
    {
        e = email.getText().toString();
        p = password.getText().toString();
        c = c_password.getText().toString();
        n = name.getText().toString();
    }

    private boolean valitation()
    {
        boolean valid = true;
        if(e.isEmpty()||!Patterns.EMAIL_ADDRESS.matcher(e).matches())
        {
//            !e.matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+")
            email.setError("enter valid email");
            valid = false;
        }


        if(p.isEmpty() || !p.equals(c) || p.length() <8)
        {
//            password.setError("enter valid password");
//            valid = false;

                if(!p.equals(c))
                {
                    password.setError("confirm password does not same");
                    valid = false;



                }

                if(p.length() < 8)
                {
                    password.setError("minimum character is 8 ");
                    valid = false;
                 }

        }

        if(c.isEmpty() || !c.equals(p) || c.length() < 8)
        {
            c_password.setError("enter valid password");
            valid = false;
        }


        if(n.isEmpty())
        {
            name.setError("please enter name");
            valid = false;
        }

        return valid;
    }

    private void signUp()
    {
        postRegisterNormalUser();
    }

    private void postRegisterNormalUser()
    {
        String BASE_URL = Constants.BASE_URL+"/api/register";
//        String BASE_URL ="http://192.168.0.102:5000/api/register";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                Toast.makeText(registerActivity.this,response.toString(),Toast.LENGTH_LONG).show();


                try {
                    JSONObject resObject  = new JSONObject(response);
                    JSONObject json2 = resObject.getJSONObject("success");    // this will return correct
                    Log.i("masukkal", "-->" + json2.getString("id"));

                    User user  = new User(
                            idR = json2.getString("id"),
                            tokenR=  json2.getString("token"),
                            nameR= json2.getString("name"),
                            emailR=json2.getString("email"),
                            uRole=json2.getString("role_type")

                    );

                    new User(nameR,tokenR,idR,emailR,uRole);
                    Log.i("masukkal", "-->" + json2.getString("id"));
                    Log.i("masukkal", "-->" + json2.getString("token"));
                    Log.i("masukkal", "-->" + json2.getString("name"));
//                    Log.i("aa",idR.toString());

                    sharedPreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("userID",idR);
                    editor.putString("userEmail",emailR);
                    editor.putString("userName",nameR);
                    editor.putString("userToken",tokenR);
                    editor.putString("userRole",uRole);

                    editor.putBoolean("userLogin",true);
                    editor.apply();


//                    CollectionReference dbUser = db.collection("user");
//                    Map <String,Object> userR = new HashMap<>();
//                    userR.put("role","NormalUser");
//                    userR.put("email",emailR);
//                    userR.put("name",nameR);
//                    userR.put("token",tokenR);
//                    userR.put("id",idR);
//
//                    db.collection("user").document(mFirebaseAuth.getUid()).set(userR);


//                    Intent i = new Intent(registerActivity.this, newUser_pickCategory_activity.class);
//                    startActivity(i);

                } catch (JSONException ex) {
                    ex.printStackTrace();
                }



                    Toast.makeText(registerActivity.this,"Congratulation Register Success, Please Login Again",Toast.LENGTH_LONG).show();
                    Intent i = new Intent(registerActivity.this, MainActivity.class);
                    startActivity(i);


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

//                duplicate = "exists";

//                Toast.makeText(registerActivity.this,error.getMessage(),Toast.LENGTH_LONG).show();
                Toast.makeText(registerActivity.this,"Account already register ! please use other email",Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            protected Map<String,String> getParams(){

                Map<String , String> params = new HashMap<String, String>();

                params.put("email",email.getText().toString().trim());
                params.put("password",password.getText().toString().trim());
                params.put("name",name.getText().toString().trim());
//                params.put("name",c_password.getText().toString().trim());
                return params;

            }
        };
        requestQueue.add(stringRequest);


    }

}
