package com.example.unievent.UserActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.unievent.Main2Activity;
import com.example.unievent.MainActivity;
import com.example.unievent.R;
import com.example.unievent.recyclerPage;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import de.hdodenhof.circleimageview.CircleImageView;

public class profile extends AppCompatActivity {


    SharedPreferences sharedPreferences;
    TextView welcome;
    Button btn_logout,btn_like,t;

    String id,first_name,last_name, email , imageURL;

    CircleImageView circleImageView;

    FirebaseAuth mFirebasAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        mFirebasAuth = FirebaseAuth.getInstance();

        AppEventsLogger.activateApp(getApplication());
        FacebookSdk.sdkInitialize(this);

        t=findViewById(R.id.t);
        t.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(profile.this, Main2Activity.class);
                startActivity(i);
            }
        });

        welcome= findViewById(R.id.Text_name);
        btn_logout = findViewById(R.id.logout);
        btn_like=findViewById(R.id.button2);
        circleImageView = findViewById(R.id.circleImageView);

        sharedPreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        id = sharedPreferences.getString("userID","");
        email = sharedPreferences.getString("userEmail","");
        first_name = sharedPreferences.getString("userName","");
       // last_name= sharedPreferences.getString("userNameBack","");
        imageURL = sharedPreferences.getString("userImage","");



//        Bundle inBundle = getIntent().getExtras();
//        String name = inBundle.get("nama_dpn").toString();
//        String belakang = inBundle.get("nama_blkng").toString();
//        String id = inBundle.get("id").toString();

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.dontAnimate();

        welcome.setText("WELCOME "+" "+first_name);
        Glide.with(profile.this).load(imageURL).into(circleImageView);

        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                mFirebasAuth.signOut();
                LoginManager.getInstance().logOut();

                Intent login = new Intent(profile.this, MainActivity.class);
                startActivity(login);
                updateUI();
                finish();
            }
        });


        btn_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent likes = new Intent(profile.this, recyclerPage.class);
                startActivity(likes);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mFirebasAuth.getCurrentUser();

        if (currentUser == null) {

            updateUI();

        }
    }

    private void updateUI() {
        Toast.makeText(profile.this,"logout",Toast.LENGTH_LONG).show();
        Intent main = new Intent(profile.this,MainActivity.class);
        startActivity(main);
//        finish();
    }
}
