package com.example.unievent.UserActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.unievent.Model.Constants;
import com.example.unievent.R;

import java.util.HashMap;
import java.util.Map;

public class user_edit_profile extends AppCompatActivity {

    SharedPreferences sharedPreferences;

    String id,email,name,token,ucheck;

    EditText user_name,user_email;

    Button submit;

    String Vname,Vemail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_edit_profile);


        sharedPreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        id = sharedPreferences.getString("userID","");
        email = sharedPreferences.getString("userEmail","");
        name = sharedPreferences.getString("userName","");
        token = sharedPreferences.getString("userToken","");
        ucheck = sharedPreferences.getString("check","");

        user_name = findViewById(R.id.user_name);
        user_email = findViewById(R.id.user_email);


        user_name.setText(name);
        user_email.setText(email);

        submit = findViewById(R.id.btn_edit_profile);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                edit();


            }
        });


    }

    private void edit()
    {
        intialize();

        if(!valitation())
        {

            Toast.makeText(user_edit_profile.this,"edit profile fail",Toast.LENGTH_LONG).show();

        }

        else
        {
            editProfile();

        }
    }


    private void intialize()
    {
        Vname = user_name.getText().toString();
        Vemail = user_email.getText().toString();

    }

    private boolean valitation()
    {
        boolean valid = true;
        if(Vname.isEmpty())
        {
            user_name.setError("enter valid email");
            valid = false;
        }


        if(Vemail.isEmpty()||!Patterns.EMAIL_ADDRESS.matcher(Vemail).matches())
        {
            user_email.setError("enter valid email");
            valid = false;
        }




        return valid;
    }



    private void editProfile()
    {
        String BASE_URL = Constants.BASE_URL+"/api/updateProfile/"+id;
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        StringRequest stringRequest = new StringRequest(Request.Method.PUT, BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(user_edit_profile.this,"updated",Toast.LENGTH_LONG).show();
                Intent i = new Intent(user_edit_profile.this, UserProfileActivity.class);
                startActivity(i);


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(user_edit_profile.this,error.getMessage(),Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            protected Map<String,String> getParams(){

                Map<String , String> params = new HashMap<String, String>();



                params.put("name",user_name.getText().toString());
                params.put("email",user_email.getText().toString());



                return params;

            }
        };
        requestQueue.add(stringRequest);
    }

}
