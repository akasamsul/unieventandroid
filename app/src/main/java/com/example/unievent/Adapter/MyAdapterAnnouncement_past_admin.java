package com.example.unievent.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.unievent.Model.Announcement;
import com.example.unievent.R;

import java.util.List;

public class MyAdapterAnnouncement_past_admin extends RecyclerView.Adapter<MyAdapterAnnouncement_past_admin.ViewHolder> {


    private List<Announcement> announcementList;
    private Context context;
    private OnItemClickListener mListener;

    public interface OnItemClickListener
    {
        void onItemClick(int position);
    }

    public void setOnClickListener(OnItemClickListener listener)
    {
        mListener = listener;
    }

    public MyAdapterAnnouncement_past_admin(List<Announcement> announcementList, Context context) {
        this.announcementList = announcementList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_admin_announcement_past,parent,false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Announcement announcement = announcementList.get(position);

        holder.textViewTitle.setText(announcement.getTitle());
        holder.textViewDesc.setText(announcement.getDescription());

    }

    @Override
    public int getItemCount() {
        return announcementList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {

        public TextView textViewTitle;
        public TextView textViewDesc;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewTitle = itemView.findViewById(R.id.TextTitle);
            textViewDesc = itemView.findViewById(R.id.TextDesc);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener!=null)
                    {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION)
                        {
                            mListener.onItemClick(position);
                        }
                    }
                }
            });


        }
    }
}
