package com.example.unievent.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.unievent.Model.Announcement;
import com.example.unievent.R;

import java.util.List;

public class MyAdapterList extends RecyclerView.Adapter<MyAdapterList.ViewHolder> {

    List<Announcement> announcementList;
    Context context;

    public MyAdapterList(List<Announcement> announcementList, Context context) {
        this.announcementList = announcementList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_list,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Announcement announcement = announcementList.get(position);
        holder.textViewTitle.setText(announcement.getTitle());
        holder.textViewDesc.setText(announcement.getDescription());
    }

    @Override
    public int getItemCount() {
        return announcementList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView textViewTitle;
        public TextView textViewDesc;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewTitle = itemView.findViewById(R.id.cat_name);
            textViewDesc = itemView.findViewById(R.id.value);
        }
    }
}
