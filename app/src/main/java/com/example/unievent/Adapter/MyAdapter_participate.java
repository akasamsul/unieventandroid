package com.example.unievent.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.unievent.Model.RegisterParticipate;
import com.example.unievent.R;

import java.util.List;

public class MyAdapter_participate extends RecyclerView.Adapter<MyAdapter_participate.ViewHolder> {


   private List<RegisterParticipate> participates;
    private Context context;

    private OnItemClickListener mListener;

    //kalau nk buat itemClick, buat ini inteface dlu, pastu baru declare,pastu baru setonitemclick
    public interface OnItemClickListener{
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener)
    {
        mListener = listener ;
    }


    public MyAdapter_participate(List<RegisterParticipate> participates, Context context) {
        this.participates = participates;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_participate,parent,false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        RegisterParticipate participate = participates.get(position);

        holder.matrix.setText(participate.getMatrix());
        holder.name.setText(participate.getName());


    }

    @Override
    public int getItemCount() {
        return participates.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {

        public TextView name,matrix;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name_participate);
            matrix = itemView.findViewById(R.id.no_matrix);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(mListener != null)
                    {
                        int position = getAdapterPosition();

                        if (position != RecyclerView.NO_POSITION)
                        {
                            mListener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }

}
