package com.example.unievent.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.unievent.Model.Event;
import com.example.unievent.R;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

//import com.squareup.picasso.Picasso;

public class MyAdapterEvent extends RecyclerView.Adapter<MyAdapterEvent.ViewHolder> {



  private List<Event> eventList;
  private Context context;
  private OnItemClickListener mListener;


    public interface OnItemClickListener
    {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener)
    {
        mListener = listener;
    }

    public MyAdapterEvent(List<Event> eventList, Context context) {
        this.eventList = eventList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_event,parent,false);

        return new ViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Event event = eventList.get(position);



        //for date
        DateFormat outputFormat = new SimpleDateFormat("EEE, dd/MM/yyyy");
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = null;
        try {
            date = inputFormat.parse(event.getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outputText = outputFormat.format(date);


        //for time
        DateFormat outputFormatTime = new SimpleDateFormat("hh.mm aa");
        DateFormat inputFormatTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date time = null;
        try {
            time = inputFormatTime.parse(event.getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String TimeDisplay = outputFormatTime.format(time);

        String year = outputText.substring(11,15);
        String day = outputText.substring(0,11);




        holder.textName.setText(event.getName());


        holder.textDate.setText(day+"\n"+year);

        holder.organizer.setText("BY:"+event.getOrganizer());

        holder.textTime.setText(TimeDisplay);

        holder.textVenue.setText(event.getVenue());

        holder.category.setText(event.getCategory2());

        Picasso.get().load(event.getImage()).into(holder.imageView);
//        Picasso.with(context).load(event.getImage()).into(holder.imageView);
    }

    @Override
    public int getItemCount()
    {
        return eventList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView textName;

        public TextView textDate;

        public TextView textTime;

        public TextView textVenue;

        public ImageView imageView;

        public  TextView category;

        public TextView organizer;



        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            textName = itemView.findViewById(R.id.name_event);

            textDate = itemView.findViewById(R.id.date_event);
            textTime = itemView.findViewById(R.id.time_event);
            textVenue = itemView.findViewById(R.id.venue_event);

            imageView = itemView.findViewById(R.id.imageView);

            category = itemView.findViewById(R.id.category_event);

            organizer = itemView.findViewById(R.id.organizer_name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mListener!=null)
                    {
                        int position = getAdapterPosition();
                        if (position !=  RecyclerView.NO_POSITION)
                        {
                            mListener.onItemClick(position);
                        }
                    }
                }
            });

        }
    }

}
