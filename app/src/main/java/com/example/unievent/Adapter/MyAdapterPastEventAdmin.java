package com.example.unievent.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.unievent.Model.Event;
import com.example.unievent.R;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MyAdapterPastEventAdmin extends RecyclerView.Adapter<MyAdapterPastEventAdmin.ViewHolder> {

    private List<Event> eventListPast;
    private Context context;
    private OnItemClickListener mListener;

    public interface OnItemClickListener
    {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener)
    {
        mListener = listener;
    }

    public MyAdapterPastEventAdmin(List<Event> eventListPast, Context context) {
        this.eventListPast = eventListPast;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_past_event_admin,parent,false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

            Event past = eventListPast.get(position);


        //for date
        DateFormat outputFormat = new SimpleDateFormat("EEE, dd/MM/yyyy");
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = null;
        try {
            date = inputFormat.parse(past.getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outputText = outputFormat.format(date);


        //for time
        DateFormat outputFormatTime = new SimpleDateFormat("hh.mm aa");
        DateFormat inputFormatTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date time = null;
        try {
            time = inputFormatTime.parse(past.getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String TimeDisplay = outputFormatTime.format(time);

        String year = outputText.substring(11,15);
        String day = outputText.substring(0,11);

        holder.textName.setText(past.getName());

        holder.textTime.setText(TimeDisplay);

        holder.textDate.setText(day+"\n"+year);


        holder.textVenue.setText(past.getVenue());

        holder.organizer.setText("BY:"+past.getOrganizer());

        holder.category.setText(past.getCategory2());

        Picasso.get().load(past.getImage()).into(holder.imageView);


    }

    @Override
    public int getItemCount() {
        return eventListPast.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {

        public TextView textName;

        public TextView textDate;

        public TextView textTime;

        public TextView textVenue;

        public ImageView imageView;

        public  TextView category;

        public TextView organizer;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            textName = itemView.findViewById(R.id.name_event_join_past_admin);
//
            textDate = itemView.findViewById(R.id.date_event_join_past_admin);

            textVenue = itemView.findViewById(R.id.venue_admin_past);

            textTime = itemView.findViewById(R.id.time_admin_past);

            organizer = itemView.findViewById(R.id.organizer_name);

            imageView = itemView.findViewById(R.id.imageView_join_past_admin);

            category = itemView.findViewById(R.id.category_event_join_past_admin);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mListener!=null)
                    {
                        int position = getAdapterPosition();
                        if (position !=  RecyclerView.NO_POSITION)
                        {
                            mListener.onItemClick(position);
                        }
                    }
                }
            });

        }
    }

}
