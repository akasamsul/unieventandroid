package com.example.unievent.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.unievent.Model.Event;
import com.example.unievent.R;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MyAdapterRecommendation extends RecyclerView.Adapter<MyAdapterRecommendation.ViewHolder> {


    private List<Event> listRecommendation;
    private Context context;
    private OnItemClickListener mListener;


    public interface OnItemClickListener
    {
        void onItemClick(int position);
    }

    public void setOnClickListener(OnItemClickListener listener)
    {
        mListener = listener;
    }

    public MyAdapterRecommendation(List<Event> listRecommendation, Context context) {
        this.listRecommendation = listRecommendation;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_recommendation,parent,false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Event eventListRecommendation = listRecommendation.get(position);


        //for date
        DateFormat outputFormat = new SimpleDateFormat("EEE, dd/MM/yyyy");
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = null;
        try {
            date = inputFormat.parse(eventListRecommendation.getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outputText = outputFormat.format(date);


        //for time
        DateFormat outputFormatTime = new SimpleDateFormat("hh.mm aa");
        DateFormat inputFormatTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date time = null;
        try {
            time = inputFormatTime.parse(eventListRecommendation.getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String TimeDisplay = outputFormatTime.format(time);

        String year = outputText.substring(11,15);
        String day = outputText.substring(0,11);


        holder.textName.setText(eventListRecommendation.getName());
//        holder.textDate.setText(outputText);
        holder.textDate.setText(day+"\n"+year);
        holder.category.setText(eventListRecommendation.getCategory2());
        holder.textVenue.setText(eventListRecommendation.getVenue());
        holder.organizer.setText("BY: "+eventListRecommendation.getOrganizer());
        holder.textTime.setText(TimeDisplay);

        Picasso.get().load(eventListRecommendation.getImage()).into(holder.imageView);


    }

    @Override
    public int getItemCount() {
        return listRecommendation.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {

        public TextView textName;
        //        public TextView textdescription;
        public TextView textDate;
        public TextView textTime;
        public TextView textVenue;
        public ImageView imageView;
        public TextView category;
        public TextView organizer;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            textName = itemView.findViewById(R.id.name_event_recommendation);
//            textdescription = itemView.findViewById(R.id.des_event);
            textDate = itemView.findViewById(R.id.date_event_recommendation);
            textTime = itemView.findViewById(R.id.time_recommendation);
            textVenue = itemView.findViewById(R.id.venue_recommendation);

            imageView = itemView.findViewById(R.id.imageView_recommendation);
            category = itemView.findViewById(R.id.category_event_recommendation);
            organizer =itemView.findViewById(R.id.organizer_name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (mListener!=null)
                    {
                        int position = getAdapterPosition();

                        if (position != RecyclerView.NO_POSITION)
                        {
                            mListener.onItemClick(position);
                        }
                    }

                }
            });



        }
    }
}
