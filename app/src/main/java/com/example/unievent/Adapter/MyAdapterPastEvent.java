package com.example.unievent.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.unievent.Model.Event;
import com.example.unievent.R;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MyAdapterPastEvent extends RecyclerView.Adapter<MyAdapterPastEvent.ViewHolder> {


    private List<Event>  listPastEvents;
    private Context context;
    private OnItemClickListener mListener;

    public interface OnItemClickListener
    {
        void onItemClick(int position);
    }

    public void setOnClickListener(OnItemClickListener listener)
    {
        mListener= listener;
    }

    public MyAdapterPastEvent(List<Event> listPastEvents, Context context) {
        this.listPastEvents = listPastEvents;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_past_event,parent,false);

        return new ViewHolder(v);


    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapterPastEvent.ViewHolder holder, int position) {

            Event past = listPastEvents.get(position);





        //for date
        DateFormat outputFormat = new SimpleDateFormat("EEE, dd/MM/yyyy");
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = null;
        try {
            date = inputFormat.parse(past.getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outputText = outputFormat.format(date);


        //for time
        DateFormat outputFormatTime = new SimpleDateFormat("hh.mm aa");
        DateFormat inputFormatTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date time = null;
        try {
            time = inputFormatTime.parse(past.getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String TimeDisplay = outputFormatTime.format(time);


        String year = outputText.substring(11,15);
        String day = outputText.substring(0,11);

       holder.textVenue.setText(past.getVenue());
        holder.organizer.setText(past.getOrganizer());
        holder.textName.setText(past.getName());
//        holder.textDate.setText(outputText);
        holder.textDate.setText(day+"\n"+year);
        holder.textTime.setText(TimeDisplay);
        holder.category.setText("BY: "+past.getCategory2());
        Picasso.get().load(past.getImage()).into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return listPastEvents.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder
    {

        public TextView textName;

        public TextView textDate;

        public TextView textTime;

        public TextView textVenue;

        public ImageView imageView;

        public TextView organizer;

        public TextView category;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            textName = itemView.findViewById(R.id.name_event_join_past);


            organizer = itemView.findViewById(R.id.organizer_name);

            textDate = itemView.findViewById(R.id.date_event_join_past);

            textTime = itemView.findViewById(R.id.time_join_past);

            textVenue = itemView.findViewById(R.id.venue_join_past);

            imageView = itemView.findViewById(R.id.imageView_join_past);

            category = itemView.findViewById(R.id.category_event_join_past);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (mListener!=null)
                    {
                        int position = getAdapterPosition();

                        if (position != RecyclerView.NO_POSITION)
                        {
                            mListener.onItemClick(position);
                        }
                    }

                }
            });

        }
    }
}
