package com.example.unievent.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.unievent.Model.Event;
import com.example.unievent.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MyAdapterHomeNoComment extends RecyclerView.Adapter<MyAdapterHomeNoComment.ViewHolder> {

    private List<Event> eventList;
    private Context context;
    private OnItemClickListener mListener;

    public interface OnItemClickListener
    {
        void onItemClick(int position);
    }

    public void setOnClickListener(OnItemClickListener listener)
    {
        mListener = listener ;
    }

    public MyAdapterHomeNoComment(List<Event> eventList, Context context) {
        this.eventList = eventList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_user_view_event,parent,false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Event event = eventList.get(position);

        holder.textName.setText(event.getName());
//        holder.textdescription.setText(event.getDescription());
        holder.textDate.setText(event.getDate());
//        holder.textLocation.setText(event.getLocation());
//        holder.textVenue.setText(event.getVenue());

        holder.category.setText(event.getCategory2());

        Picasso.get().load(event.getImage()).into(holder.imageView);

    }

    @Override
    public int getItemCount() {

        return eventList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {

        public TextView textName;
        //        public TextView textdescription;
        public TextView textDate;
        //        public TextView textLocation;
//        public TextView textVenue;
        public ImageView imageView;

        public TextView category;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            textName = itemView.findViewById(R.id.name_event);
//            textdescription = itemView.findViewById(R.id.des_event);
            textDate = itemView.findViewById(R.id.date_event);
//            textLocation = itemView.findViewById(R.id.location_event);
//            textVenue = itemView.findViewById(R.id.venue_event);

            imageView = itemView.findViewById(R.id.imageView);

            category = itemView.findViewById(R.id.category_event);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener!=null)
                    {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION)
                        {
                            mListener.onItemClick(position);
                        }
                    }
                }
            });

        }
    }
}
