package com.example.unievent.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.unievent.Model.Event;
import com.example.unievent.R;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MyAdapterSearchEvent extends RecyclerView.Adapter<MyAdapterSearchEvent.ViewHolder> implements Filterable {


    private List<Event> listSeacrhEvent;
    private List<Event> listSeacrhEventFull;
    private Context context;
    private OnItemClickListener mListener;

    public interface OnItemClickListener
    {
        void onItemClick(int position);
    }

    public void setOnClickListener(OnItemClickListener listener)
    {
        mListener= listener;
    }

    public MyAdapterSearchEvent(List<Event> listSeacrhEvent, Context context) {
        this.listSeacrhEvent = listSeacrhEvent;
        this.context = context;
        listSeacrhEventFull = new ArrayList<>(listSeacrhEvent);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_search_event,parent,false);

        return new ViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

       final Event searchEvent = listSeacrhEvent.get(position);


        //for date
        DateFormat outputFormat = new SimpleDateFormat("EEE, dd/MM/yyyy");
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = null;
        try {
            date = inputFormat.parse(searchEvent.getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outputText = outputFormat.format(date);


        //for time
        DateFormat outputFormatTime = new SimpleDateFormat("hh.mm aa");
        DateFormat inputFormatTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date time = null;
        try {
            time = inputFormatTime.parse(searchEvent.getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String TimeDisplay = outputFormatTime.format(time);


        String year = outputText.substring(11,15);
        String day = outputText.substring(0,11);

        holder.textName.setText(searchEvent.getName());
//        holder.textDate.setText(outputText);
        holder.textDate.setText(day+"\n"+year);
        holder.textVenue.setText(searchEvent.getVenue());
        holder.textTime.setText(TimeDisplay);
        holder.category.setText(searchEvent.getCategory2());
        holder.organizer.setText("BY:"+searchEvent.getOrganizer());
        Picasso.get().load(searchEvent.getImage()).fit().into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        if(listSeacrhEvent != null)
        {
            return listSeacrhEvent.size();
        }

        return 0;

    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {

        public TextView textName;

        public TextView textDate;

        public TextView textTime;

        public TextView textVenue;

        public ImageView imageView;

        public TextView organizer;

        public TextView category;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            textName = itemView.findViewById(R.id.name_event_search);
//
            organizer = itemView.findViewById(R.id.organizer_name);

            textDate = itemView.findViewById(R.id.date_event_search);

            textTime = itemView.findViewById(R.id.time_search);

            textVenue = itemView.findViewById(R.id.venue_search);

            imageView = itemView.findViewById(R.id.imageView_search);

            category = itemView.findViewById(R.id.category_event_search);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener!=null)
                    {
                        int position = getAdapterPosition();

                        if (position != RecyclerView.NO_POSITION)
                        {
                            mListener.onItemClick(position);
                        }
                    }
                }
            });

        }
    }

    @Override
    public Filter getFilter() {
        return eventFilter;
    }

    private Filter eventFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            List<Event> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(listSeacrhEventFull);
            }

            else
            {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (Event item  : listSeacrhEventFull)
                {
                    if(item.getName().toLowerCase().contains(filterPattern) || item.getDate().toLowerCase().contains(filterPattern)||item.getCategory2().toLowerCase().contains(filterPattern)
                        || item.getOrganizer().toLowerCase().contains(filterPattern) || item.getVenue().contains(filterPattern)
                        )
                    {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            listSeacrhEvent.clear();
            listSeacrhEvent.addAll((List)results.values);
            notifyDataSetChanged();
        }
    };
}
