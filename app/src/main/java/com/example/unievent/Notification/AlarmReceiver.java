package com.example.unievent.Notification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;

import com.example.unievent.MainActivity;
import com.example.unievent.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.app.NotificationManager.IMPORTANCE_DEFAULT;

public class AlarmReceiver extends BroadcastReceiver {

    SharedPreferences sharedPreferences;
    private static final String CHANNEL_ID = "com.singhajit.notificationDemo.channelId";



    @Override
    public void onReceive(Context context, Intent intent) {



        sharedPreferences =context.getSharedPreferences("Notification", Context.MODE_PRIVATE);
        String user_event_id = sharedPreferences.getString("event_id","");
        String   nameEvent = sharedPreferences.getString("name_event","");
        String  dateEvent = sharedPreferences.getString("date","");
        String   mdated = sharedPreferences.getString("dated","");
        String   mtimed = sharedPreferences.getString("timed","");
        String location = sharedPreferences.getString("location","");
        String venue = sharedPreferences.getString("venue","");

        Intent notificationIntent = new Intent(context, MainActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(notificationIntent);

        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);



        //convert time to am/pm
        DateFormat outputFormatTime = new SimpleDateFormat("hh.mm aa");
        DateFormat inputFormatTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date time = null;
        try {
            time = inputFormatTime.parse(dateEvent);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String TimeDisplay = outputFormatTime.format(time);





        Notification.Builder builder = new Notification.Builder(context);

        Notification notification = builder.setContentTitle("Don't Forget Your Event Tomorrow !!")
                .setContentText(nameEvent +" " + "on" + " " + TimeDisplay + " "+ "at" + " " + venue )
                .setTicker("New Message Alert!")
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentIntent(pendingIntent).build();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(CHANNEL_ID);
        }

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    CHANNEL_ID,
                    "NotificationDemo",
                    IMPORTANCE_DEFAULT
            );
            notificationManager.createNotificationChannel(channel);
        }

        int idNofitifcation =Integer.parseInt(user_event_id);
        notificationManager.notify(idNofitifcation, notification);
    }




}
