package com.example.unievent.Recommendation;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.unievent.Adapter.MyAdapterRecommendation;
import com.example.unievent.Details.Detail_user_home_NoComment;
import com.example.unievent.Model.Constants;
import com.example.unievent.Model.Event;
import com.example.unievent.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Recommendation extends AppCompatActivity implements MyAdapterRecommendation.OnItemClickListener {

    private static final String URL_DATA = Constants.BASE_URL+"/api/recommendationToUser";
    private RecyclerView recyclerView;

    private MyAdapterRecommendation myAdapterRecommendation;

    private List<Event> eventListRecommendation;
    SharedPreferences sharedPreferences;

    String id,email,name,token;
    String fbid,fbeamail,fbname,fuid;
    String ucheck,fcheck;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recommendation);


        sharedPreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        id = sharedPreferences.getString("userID","");
//        email = sharedPreferences.getString("userEmail","");
//        name = sharedPreferences.getString("userName","");
//        token = sharedPreferences.getString("userToken","");
        ucheck = sharedPreferences.getString("check","");
//
//
        sharedPreferences = getSharedPreferences("userFacebook", Context.MODE_PRIVATE);
        fuid = sharedPreferences.getString("ID","");
//        fbeamail = sharedPreferences.getString("fbEmail","");
//        fbname = sharedPreferences.getString("fbName","");
//        fbid = sharedPreferences.getString("fbID","");
        fcheck = sharedPreferences.getString("check","");


        recyclerView = findViewById(R.id.recyclerViewRecommendation);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        eventListRecommendation = new ArrayList<>();

        loadRecyclerViewDataRecommmendation();

    }


    private void loadRecyclerViewDataRecommmendation(){
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("LOADING DATA...");
        progressDialog.show();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_DATA, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                progressDialog.dismiss();
                try {
                    JSONArray j = new JSONArray(response);

                    for (int i =0; i<j.length(); i++)
                    {
                        JSONObject obj= j.getJSONObject(i);

                        JSONArray obj2 = obj.getJSONArray("categories");
                        ArrayList category = new ArrayList();
                        String cate ="";
                        for(int k =0; k<obj2.length(); k++)
                        {
                            JSONObject objCategory = obj2.getJSONObject(k);
                            String catName =  objCategory.getString("category");


                            cate += catName+", ";


                        }
                        Event event = new Event(
                                obj.getString("id"),
                                obj.getString("name"),
                                obj.getString("description"),
                                obj.getString("date"),
                                obj.getString("venue"),
                                obj.getString("location"),
                                obj.getString("image"),
                                obj.getString("dated"),
                                obj.getString("timed"),
                                cate,
                                obj.getString("organizer")


                        );

                        Log.i("fdsafd",cate);
                        eventListRecommendation.add(event);

                    }

                    myAdapterRecommendation = new MyAdapterRecommendation(eventListRecommendation,getApplicationContext());
                    recyclerView.setAdapter(myAdapterRecommendation);
                    myAdapterRecommendation.setOnClickListener(Recommendation.this);


                } catch (Exception e) {
                    e.printStackTrace();
                }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(Recommendation.this,error.getMessage(),Toast.LENGTH_LONG).show();
            }
        })
        {
            @Override
            protected Map<String,String> getParams(){

                Map<String , String> params = new HashMap<String, String>();

                if(fcheck.equals("login2"))
                {
                    params.put("user_id",fuid);

                }

                else
                {
                    params.put("user_id",id);

                }

                Log.i("tefdsaf",params.toString());
                return params;

            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    @Override
    public void onItemClick(int position) {
//        Intent i = new Intent(Recommendation.this, Detail_user_view_event.class);
        Intent i = new Intent(Recommendation.this, Detail_user_home_NoComment.class);
        Event  clickedItem = eventListRecommendation.get(position);

//        sharedPreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
//        id = sharedPreferences.getString("userID","");
//        email = sharedPreferences.getString("userEmail","");
//        name = sharedPreferences.getString("userName","");
//        token = sharedPreferences.getString("userToken","");
//        event_id =sharedPreferences.getString("userToken","");


//        sharedPreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        editor.putString("userID",id);
//        editor.putString("userEmail",email);
//        editor.putString("userName",name);
//        editor.putString("userToken",token);
////        editor.putString("user_event_id",String.valueOf(position));
//
//        editor.putBoolean("userLogin",true);
//        editor.apply();





        i.putExtra("id",clickedItem.getId());
        i.putExtra("image",clickedItem.getImage());
        i.putExtra("name",clickedItem.getName());
        i.putExtra("description",clickedItem.getDescription());
        i.putExtra("date",clickedItem.getDate());
        i.putExtra("dated",clickedItem.getDateddd());
        i.putExtra("timed",clickedItem.getTimeddd());
        i.putExtra("venue",clickedItem.getVenue());
        i.putExtra("location",clickedItem.getLocation());
        i.putExtra("organizer",clickedItem.getOrganizer());


        sharedPreferences = getSharedPreferences("event", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("name_event",clickedItem.getName());
        editor.putString("date",clickedItem.getDate());
        editor.putString("dated",clickedItem.getDateddd());
        editor.putString("timed",clickedItem.getTimeddd());
        editor.putString("event_id",clickedItem.getId());
        editor.putString("location",clickedItem.getLocation());
        editor.putString("organizer",clickedItem.getOrganizer());

        editor.apply();

        startActivity(i);
    }
}
