package com.example.unievent.Details;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.unievent.AdminActivity.Admin_profile_activity;
import com.example.unievent.AdminActivity.Admin_update_announcement;
import com.example.unievent.Model.Constants;
import com.example.unievent.R;

public class DetailActivity_Announcement_admin extends AppCompatActivity {

    TextView title,des;
    Button update, delete;
    String id,name,description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail__announcement_admin);

        Intent intent = getIntent();
         id = intent.getStringExtra("id");
         name = intent.getStringExtra("title");
         description = intent.getStringExtra("description");

        title = findViewById(R.id.title_announcement_detail);
        des = findViewById(R.id.description_announcement_detail);
        update = findViewById(R.id.update_announcement);
        delete = findViewById(R.id.delet_announcement);

        title.setText(name);
        des.setText(description);


        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alt = new AlertDialog.Builder(DetailActivity_Announcement_admin.this);
                alt.setMessage("Do you want to delete this news ?").setCancelable(false)
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleletFunction();
                    }
                })
                        .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alertDialog = alt.create();
                alertDialog.setTitle("DELETE FLASH NEWS");
                alertDialog.show();

            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 Intent i = new Intent(DetailActivity_Announcement_admin.this, Admin_update_announcement.class);

                i.putExtra("id",id);
                i.putExtra("title",name);
                i.putExtra("description",description);


                startActivity(i);
            }
        });




    }

    public void deleletFunction()
    {
        String deteleUrl =  Constants.BASE_URL+"/api/deleteAnnouncement/"+id;
//        String BASE_URL = Constants.BASE_URL+"/api/createEvent";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, deteleUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(DetailActivity_Announcement_admin.this,"your news has been deleted",Toast.LENGTH_LONG).show();
                Intent i = new Intent(DetailActivity_Announcement_admin.this, Admin_profile_activity.class);
                startActivity(i);
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(Admin_create_event_activity.this,error.getMessage(),Toast.LENGTH_LONG).show();

            }
        });

        requestQueue.add(stringRequest);
    }
}
