package com.example.unievent.Details;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.unievent.R;

public class userparticipate_for_admin extends AppCompatActivity {

    TextView mname,mmatrix, mphone,mcourse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userparticipate_for_admin);


        Intent intent = getIntent();
        String name = intent.getStringExtra("name");
        String matrix = intent.getStringExtra("matrix");
        String phone = intent.getStringExtra("nophone");
        String course = intent.getStringExtra("course");

        mname = findViewById(R.id.name_detail);
        mmatrix = findViewById(R.id.matrix_detail);
        mphone = findViewById(R.id.phone_detail);
        mcourse = findViewById(R.id.course_detail);


        mname.setText(name);
        mmatrix.setText(matrix);
        mphone.setText(phone);
        mcourse.setText(course);



    }
}
