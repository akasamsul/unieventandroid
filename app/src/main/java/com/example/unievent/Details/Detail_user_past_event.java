package com.example.unievent.Details;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.unievent.R;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Detail_user_past_event extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    String id,email,name,token,user_event_id,ucheck;
    String fid,fbid,femail,fname,fcheck;
    String comment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_user_past_event);

        sharedPreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        id = sharedPreferences.getString("userID","");
        email = sharedPreferences.getString("userEmail","");
        name = sharedPreferences.getString("userName","");
        token = sharedPreferences.getString("userToken","");
        ucheck = sharedPreferences.getString("check","");
//        user_event_id = sharedPreferences.getString("user_event_id","");


        sharedPreferences = getSharedPreferences("userFacebook", Context.MODE_PRIVATE);
        fid = sharedPreferences.getString("ID","");
        femail = sharedPreferences.getString("fbEmail","");
        fname = sharedPreferences.getString("fbName","");
        fcheck = sharedPreferences.getString("check","");

        Intent i = getIntent();
        user_event_id = i.getStringExtra("id");
        String image = i.getStringExtra("image");
        String title = i.getStringExtra("name");
        String description = i.getStringExtra("description");
        String date = i.getStringExtra("date");
        String location = i.getStringExtra("location");
        String venue = i.getStringExtra("venue");
        comment = i.getStringExtra("comment");

        ImageView imageView = findViewById(R.id.image_detail_user_past);

        TextView textViewT = findViewById(R.id.title_detail_user_past);
        TextView textViewD = findViewById(R.id.description_detail_user_past);
        TextView textViewDate = findViewById(R.id.date_detail_user_past);
        TextView textViewVenue = findViewById(R.id.venue_detail_user_past);
        TextView textViewLocation =findViewById(R.id.location_detail_user_past);
        TextView tComment = findViewById(R.id.comment_past);


//        Picasso.get().load(image).fit().centerInside().into(imageView);

        Picasso.get().load(image).into(imageView);

        DateFormat outputFormat = new SimpleDateFormat("EEE, dd/MM/yyyy hh.mm aa");
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date1 = null;
        try {
            date1 = inputFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outputText = outputFormat.format(date1);



        textViewT.setText(title);
        textViewD.setText(description);

        textViewDate.setText(outputText);
        textViewVenue.setText(venue);
        textViewLocation.setText(location);
        tComment.setText(comment);

    }
}
