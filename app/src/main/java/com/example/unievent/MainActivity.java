package com.example.unievent;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.unievent.AdminActivity.AdminLoginActivity;
import com.example.unievent.Model.Constants;
import com.example.unievent.Model.User;
import com.example.unievent.UserActivity.UserProfileActivity;
import com.example.unievent.UserActivity.newUser_pickCategory_activity;
import com.example.unievent.UserActivity.registerActivity;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    TextView adminLOgin ;
    EditText pass,emailT;
    Button btn_login, btn_register;

    SessionManagement session;

    //test
    Button test;

    Button fbbtn;
    SharedPreferences sharedPreferences;

    FirebaseAuth mFirebaseAuth;
    FirebaseFirestore db;
    String idR,nameR,emailR,fbid,role;


    private static final String TAG = "keluarla";


    private CallbackManager callbackManager;
    private ProfileTracker profileTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        session = new SessionManagement(getApplicationContext());
//        Toast.makeText(getApplicationContext(), "User Login Status: " + session.isLoggedIn(), Toast.LENGTH_LONG).show();

        if(session.isLoggedIn())
        {
            Intent intent = new Intent(MainActivity.this, UserProfileActivity.class);
            startActivity(intent);
            finish();
        }






        //test
        test= findViewById(R.id.test);

        test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, newUser_pickCategory_activity.class);
                startActivity(i);
            }
        });

        mFirebaseAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        adminLOgin = findViewById(R.id.AdminLogin);
        btn_login = findViewById(R.id.login);
        btn_register =findViewById(R.id.login2);

        fbbtn=findViewById(R.id.login_button);

        pass = findViewById(R.id.password);
        emailT = findViewById(R.id.email);


        callbackManager = CallbackManager.Factory.create();







//        loginButton.setReadPermissions("email", "public_profile");
//        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
//            @Override
//            public void onSuccess(LoginResult loginResult) {
//                Log.d(TAG, "facebook:onSuccess:" + loginResult);
//                handleFacebookAccessToken(loginResult.getAccessToken());
//
//
////
//            }
//
//            @Override
//            public void onCancel() {
//                Log.d(TAG, "facebook:onCancel");
//                // ...
//            }
//
//            @Override
//            public void onError(FacebookException error) {
//                Log.d(TAG, "facebook:onError", error);
//                // ...
//            }
//        });




        fbbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 LoginManager.getInstance().logInWithReadPermissions(MainActivity.this, Arrays.asList("email","public_profile"));
                 LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                     @Override
                     public void onSuccess(LoginResult loginResult) {
                         Log.d(TAG, "facebook:onSuccess:" + loginResult);
                         handleFacebookAccessToken(loginResult.getAccessToken());


//
                     }

                     @Override
                     public void onCancel() {
                         Log.d(TAG, "facebook:onCancel");
                         // ...
                     }

                     @Override
                     public void onError(FacebookException error) {
                         Log.d(TAG, "facebook:onError", error);
                         // ...
                     }
                 });
            }
        });


       // LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile"));
//        loginButton.setReadPermissions(Arrays.asList("email","public_profile"));

        checkLoginStatus();

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {

            }
        };

        profileTracker.startTracking();

//        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
//            @Override
//            public void onSuccess(LoginResult loginResult) {
//                Profile profile = Profile.getCurrentProfile();
//                checkLoginStatus();
//            }
//
//            @Override
//            public void onCancel() {
//
//            }
//
//            @Override
//            public void onError(FacebookException error) {
//
//            }
//        });



        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                postNormalUserLogin();
            }
        });


        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, registerActivity.class);
                startActivity(i);
            }
        });

        adminLOgin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, AdminLoginActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
            }
        });

    }// end on create


    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mFirebaseAuth.getCurrentUser();

        if (currentUser != null) {

//            updateUI();

        }
    }

//    private void updateUI() {
//        Toast.makeText(MainActivity.this,"nice",Toast.LENGTH_LONG).show();
//    }



    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mFirebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mFirebaseAuth.getCurrentUser();
                            loaduserProfile(AccessToken.getCurrentAccessToken());
//                            updateUI();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(MainActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
//                            updateUI();
                        }

                        // ...
                    }
                });
    }














//    @Override
//    protected void onResume()
//    {
//        super.onResume();
//        Profile profile = Profile.getCurrentProfile();
//        checkLoginStatus();
//    }
//
//    @Override
//    protected void onPause()
//    {
//        super.onPause();
//    }
//
//    @Override
//    protected void onStop()
//    {
//        super.onStop();
//        profileTracker.stopTracking();
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        callbackManager.onActivityResult(requestCode,resultCode,data);
        super.onActivityResult(requestCode, resultCode, data);
    }


    private void loaduserProfile(final AccessToken newAccessToken)
    {
        final GraphRequest request= GraphRequest.newMeRequest(newAccessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {

                try {







                    String name = object.getString("name");
                    String email = object.getString("email");
                    String fbid = object.getString("id");
                    String image_url = "https://graph.facebook.com/"+fbid+"/picture?type=normal";

                    sharedPreferences = getSharedPreferences("facebookDisplay", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("facebookID",fbid);
                    editor.putString("facebookEmail",email);
                    editor.putString("facebookName",name);
                    editor.putString("facebookImage",image_url);

                    editor.putString("userResponse",response.getRawResponse());
                    editor.putString("check","login1");
                    editor.putBoolean("facebookLogin",true);
                    editor.apply();


                    postUserFacebook(object);























//                    Intent i = new Intent(MainActivity.this,profile.class);
//                    Intent i = new Intent(MainActivity.this, UserProfileActivity.class);
//                    //i.putExtra("response",response.getRawResponse());
//                    startActivity(i);


                    CollectionReference dbUser = db.collection("user");
                    Map <String,Object> userR = new HashMap<>();
                    userR.put("role","FacebookUser");
                    userR.put("email",email);
                    userR.put("name",name);

                    userR.put("id",fbid);
//                      error sharedpref
//                    db.collection("user").document(mFirebaseAuth.getUid()).set(userR);




//                    JSONObject resObject = response.getJSONObject();
//                    JSONObject json2 = resObject.getJSONObject("likes");    // this will return correct
//                    JSONArray jsonArray = json2.getJSONArray("data");
//
//                        for (int i = 0; i < jsonArray.length(); i++)
//                          {
//
//                            JSONObject jsonObject = jsonArray.getJSONObject(i);
//
//                              Log.i("ID", "-->" + jsonObject.getString("id"));
//                              Log.i("ID", "-->" + jsonObject.getString("category"));
//
//                                    ListItem item = new ListItem(jsonObject.getString("category"),jsonObject.getString("id"));
//                                    listItems.add(item);
//
//                          }
////
//                            adapter = new MyAdapter(listItems,getApplicationContext());
//                            recyclerView.setAdapter(adapter);
//                        }





//                    Intent main = new Intent(MainActivity.this,profile.class);
//
//                    main.putExtra("nama_dpn",first_name);
//                    main.putExtra("response",response.getRawResponse());
//                    main.putExtra("nama_blkng",last_name);
//                    main.putExtra("email",email);
//                    main.putExtra("id",id);
//
//                    startActivity(main);


                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }





            }


        });


        Bundle parameters = new Bundle();
        parameters.putString("fields","id,name,email,likes.limit(100){category}");
        request.setParameters(parameters);
        request.executeAsync();
    }


//    private void postUserFacebook(JSONObject data)
//    {
//        String BASE_URL = Constants.BASE_URL+"/api/fbCreate";
//        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL, data, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject response) {
//                Toast.makeText(getApplicationContext(), response.toString(), Toast.LENGTH_LONG).show();
//
//                  try {
//                      JSONObject json2 = response.getJSONObject("success");
//                          // this will return correct
//
//                    User user  = new User(
//                         idR = json2.getString("id"),
//                            fbid = json2.getString("fbid"),
//                            nameR= json2.getString("name"),
//                            emailR=json2.getString("email")
//                    );
//
//                    new User(idR,fbid,nameR,emailR);
//                    Log.i("masukkal", "-->" + json2.getString("id"));
//                    Log.i("masukkal", "-->" + json2.getString("token"));
//                    Log.i("masukkal", "-->" + json2.getString("name"));
////                    Log.i("aa",idR.toString());
//
//                    sharedPreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
//                    SharedPreferences.Editor editor = sharedPreferences.edit();
//                    editor.putString("userID",idR);
//                    editor.putString("userEmail",emailR);
//                    editor.putString("userName",nameR);
//
//                    editor.putBoolean("userLogin",true);
//                    editor.apply();
//
//
//
//                } catch (JSONException ex) {
//                    ex.printStackTrace();
//                }
//
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
//            }
//        })
//        {
//             @Override
//             public Map<String,String> getHeaders() throws AuthFailureError {
//                HashMap<String , String> headers = new HashMap<>();
//                headers.put("Content-Type","application/json; charset=utf8");
//                return headers;
//             }
//        };
//
//        requestQueue.add(jsonObjectRequest);
//    }







    private void postUserFacebook(JSONObject data)
    {
        String BASE_URL = Constants.BASE_URL+"/api/fbCreate";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL, data, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
//                Toast.makeText(getApplicationContext(), response.toString(), Toast.LENGTH_LONG).show();

                try {
                    JSONArray j = response.getJSONArray("success");
                    // this will return correct

                    for (int i =0; i<j.length(); i++)
                    {

                        JSONObject obj = j.getJSONObject(i);
                        User userFacebook = new User(
                                idR=obj.getString("id"),
                                nameR=obj.getString("name"),
                                emailR=obj.getString("email"),
                                fbid=obj.getString("fbid"),
                                role=obj.getString("role_type")





                        );
                        Log.i("masukkal",idR);

//                        new User(idR,fbid,nameR,emailR,role);
                    }

//                        User user  = new User(
//                            idR = json2.getString("id"),
//                            fbid = json2.getString("fbid"),
//                            nameR= json2.getString("name"),
//                            emailR=json2.getString("email")



//                    Log.i("masukkal", "-->" + json2.getString("id"));
//                    Log.i("masukkal", "-->" + json2.getString("token"));
//                    Log.i("masukkal", "-->" + json2.getString("name"));
//                    Log.i("aa",idR.toString());

                    sharedPreferences = getSharedPreferences("userFacebook", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("ID",idR);
                    editor.putString("fbEmail",emailR);
                    editor.putString("fbName",nameR);
                    editor.putString("fbID",fbid);
                    editor.putString("fbRole",role);
                    editor.putString("check","login2");

                    editor.putBoolean("fbLogin",true);
                    editor.apply();





                    String BASE_URL = Constants.BASE_URL+"/api/checkCategoryUser";
                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
//                            Toast.makeText(MainActivity.this,response.toString(),Toast.LENGTH_LONG).show();

                            try {
                                JSONObject jresponse = new JSONObject(response);
                                check = jresponse.getString("result");
                                Log.i("masuk",check.toString());

                                if (check.equals("false"))
                                {
                                    Intent i = new Intent(MainActivity.this,UserRegisterCategory.class);
                                    startActivity(i);
                                }
                                else{

                                    Intent i = new Intent(MainActivity.this,UserProfileActivity.class);
                                    startActivity(i);

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(MainActivity.this,error.getMessage(),Toast.LENGTH_LONG).show();

                        }
                    }){
                        @Override
                        protected Map<String,String> getParams(){

                            Map<String , String> params = new HashMap<String, String>();




                            params.put("user_id",idR);







                            Log.i("masukla",params.toString());

                            return params;

                        }
                    };
                    requestQueue.add(stringRequest);



                } catch (JSONException ex) {
                    ex.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        })
        {
            @Override
            public Map<String,String> getHeaders() throws AuthFailureError {
                HashMap<String , String> headers = new HashMap<>();
                headers.put("Content-Type","application/json; charset=utf8");
                return headers;
            }
        };

        requestQueue.add(jsonObjectRequest);
    }






    String id;
    String email;
    String name;
    String token;
    String uRole;


    private void postNormalUserLogin()
    {
        String BASE_URL =Constants.BASE_URL+"/api/login";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                try {
                    //amebek data response laravel
                    JSONObject resObject  = new JSONObject(response);
                    JSONObject json2 = resObject.getJSONObject("success");    // this will return correct
                    Log.i("masukkal", "-->" + json2.getString("id"));



                    User user  = new User(
                            id = json2.getString("id"),
                            token=  json2.getString("token"),
                            name= json2.getString("name"),
                            email=json2.getString("email"),
                            uRole=json2.getString("role_type")
                    );

                    new User(name,token,id,email,uRole);
                    Log.i("masukkal", "-->" + json2.getString("id"));
                    Log.i("masukkal", "-->" + json2.getString("token"));
                    Log.i("masukkal", "-->" + json2.getString("name"));
                    Log.i("aa",id.toString());

                    sharedPreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("userID",id);
                    editor.putString("userEmail",email);
                    editor.putString("userName",name);
                    editor.putString("userRole",uRole);
                    editor.putString("userToken",token);
                    editor.putString("check","login3");

                    editor.putBoolean("userLogin",true);
                    editor.apply();


                    session.createLoginSession(email,id);

                    checkHasCategory();





//                    Toast.makeText(MainActivity.this,response.toString(),Toast.LENGTH_LONG).show();
//                    Intent i = new Intent(MainActivity.this,UserProfileActivity.class);
//                    startActivity(i);



                } catch (JSONException ex) {
                    ex.printStackTrace();
                }


















//


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this,error.getMessage(),Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            protected Map<String,String> getParams(){

                    Map<String , String> params = new HashMap<String, String>();

                    params.put("email",emailT.getText().toString().trim());
                    params.put("password",pass.getText().toString().trim());
                    return params;

            }
        };
        requestQueue.add(stringRequest);
    }

    private  void checkLoginStatus()
    {
        if (AccessToken.getCurrentAccessToken()!=null){

            loaduserProfile(AccessToken.getCurrentAccessToken());
        }
    }

String check;

    private void checkHasCategory()
    {

        String BASE_URL = Constants.BASE_URL+"/api/checkCategoryUser";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                Toast.makeText(MainActivity.this,response.toString(),Toast.LENGTH_LONG).show();

                try {
                    JSONObject jresponse = new JSONObject(response);
                    check = jresponse.getString("result");
                    Log.i("masuk",check.toString());

                    if (check.equals("false"))
                    {
                        Intent i = new Intent(MainActivity.this,UserRegisterCategory.class);
                        startActivity(i);
                        finish();
                    }
                    else{

                        Intent i = new Intent(MainActivity.this,UserProfileActivity.class);
                        startActivity(i);
                        finish();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this,error.getMessage(),Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            protected Map<String,String> getParams(){

                Map<String , String> params = new HashMap<String, String>();




                    params.put("user_id",id);







                Log.i("masukla",params.toString());

                return params;

            }
        };
        requestQueue.add(stringRequest);

    }




}//end of activity
