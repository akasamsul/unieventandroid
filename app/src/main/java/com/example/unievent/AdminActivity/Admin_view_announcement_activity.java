package com.example.unievent.AdminActivity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.example.unievent.Adapter.PagerAdapterAdminAnnouncement;
import com.example.unievent.Fragment.TabAdminViewAnnouncementPast;
import com.example.unievent.Fragment.TabAdminViewAnnouncemetCurrent;
import com.example.unievent.R;
import com.google.android.material.tabs.TabLayout;

public class Admin_view_announcement_activity extends AppCompatActivity  {

    private TabLayout tabLayout;
    private ViewPager viewPager;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_view_announcement_activity);



        viewPager = findViewById(R.id.viewPager);
        tabLayout = findViewById(R.id.tabLayout);

        setUpViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });





    }

    private void setUpViewPager(ViewPager viewPager) {

        PagerAdapterAdminAnnouncement pagerAdapter = new PagerAdapterAdminAnnouncement(getSupportFragmentManager());

        pagerAdapter.addFragment(new TabAdminViewAnnouncemetCurrent(),"CURRENT FLASH NEWS");
        pagerAdapter.addFragment(new TabAdminViewAnnouncementPast(),"PAST FLASH NEWS");
        viewPager.setAdapter(pagerAdapter);
    }


}
