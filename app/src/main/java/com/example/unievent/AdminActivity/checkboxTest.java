package com.example.unievent.AdminActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.unievent.Model.Category;
import com.example.unievent.Model.Constants;
import com.example.unievent.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class checkboxTest extends AppCompatActivity {


    List<Category> selection = new ArrayList<>();
    TextView final_text;
    CheckBox ayam1,ikan1,itik1;

    Button reset;
    String ayam,ikan,itik;

//    Category cat = new Category();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkbox_test);

        final_text = findViewById(R.id.final_ressult);

        ayam1 = findViewById(R.id.ayam);
        ikan1 = findViewById(R.id.ikan);
        itik1 = findViewById(R.id.itik);

        ayam1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (ayam1.isChecked() && !ikan1.isChecked() && !itik1.isChecked())
                {
                    ayam = "10";
                }
                else if(ayam1.isChecked() && ikan1.isChecked() && !itik1.isChecked() ||
                        ayam1.isChecked() && itik1.isChecked() && !ikan1.isChecked())
                {
                    ayam = "5";
                }
                else
                {
                    ayam="1";
                }
            }
        });

        reset = findViewById(R.id.reset);

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println(ayam);
            }
        });

    }


    public void selectItem(View view)
    {



        boolean checked = ((CheckBox)view).isChecked();

        switch (view.getId())
        {
            case R.id.ayam :

                if (checked)
                {
                    Category cat = new Category("Arts & Entertainment");

                    selection.add(cat);
                }

                else
                {
                    for(int i = 0 ; i<selection.size(); i++)
                    {
                        if (selection.get(i).getCategory().equals("Arts & Entertainment"))
                        {
                            selection.remove(i);
                        }
                    }
//                    Category cat = new Category("Arts & Entertainment");
//                    selection.remove("Arts & Entertainment");
                }

                break;

            case R.id.ikan :

                if (checked)
                {
                    Category cat = new Category("Education");
                    selection.add(cat);
                }

                else
                {
                    for(int i = 0 ; i<selection.size(); i++)
                    {
                        if (selection.get(i).getCategory().equals("Education"))
                        {
                            selection.remove(i);
                        }
                    }
//                    selection.remove("Education");
                }

                break;

            case R.id.itik :

                if (checked)
                {
                    Category cat = new Category("Finance");
                    selection.add(cat);
                }

                else
                {
                    for(int i = 0 ; i<selection.size(); i++)
                    {
                        if (selection.get(i).getCategory().equals("Finance"))
                        {
                            selection.remove(i);
                        }
                    }
//                    selection.remove("Finance");
                }

                break;




        }

    }
    String newDatarray;
    public void finalselection(View view)
    {

//        Toast.makeText(this,selection.toString(),Toast.LENGTH_LONG).show();
        String final_selection = "";

        for(int i = 0 ; i<selection.size(); i++)
        {
            System.out.println(selection.get(i).getCategory());
//            final_selection = final_selection + Selections + "\n";
        }

        final_text.setText(final_selection);


        Gson gson = new Gson();
         newDatarray = gson.toJson(selection);

        Toast.makeText(this,newDatarray.toString(),Toast.LENGTH_LONG).show();
Log.i("masuk",newDatarray.toString());
        String BASE_URL = Constants.BASE_URL+"/api/createCategoryEvent";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                Toast.makeText(Admin_create_announcement_activity.this,response.toString(),Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(checkboxTest.this,error.getMessage(),Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            protected Map<String,String> getParams(){

                Map<String , String> params = new HashMap<String, String>();

                params.put("array",newDatarray);

                Log.i("masukla",params.toString());

                return params;

            }
        };
        requestQueue.add(stringRequest);




    }


}
