package com.example.unievent.AdminActivity;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.RequestQueue;
import com.example.unievent.Adapter.MyAdapterEvent;
import com.example.unievent.Adapter.PagerAdapterAdminEvent;
import com.example.unievent.Fragment.TabPastEventAdmin;
import com.example.unievent.Fragment.TabUpComingEventAdmin;
import com.example.unievent.Model.Constants;
import com.example.unievent.Model.Event;
import com.example.unievent.R;
import com.google.android.material.tabs.TabLayout;

import java.util.List;

public class Admin_view_event_activity extends AppCompatActivity  {

//    implements MyAdapterEvent.OnItemClickListener
    private TabLayout tabLayout;
    private ViewPager viewPager;

//    public PagerAdapterAdminEvent pagerAdapter;

    private RequestQueue mQueue;

    SharedPreferences sharedPreferences;
    String id,email,token,Ename;


    private static final String URL_DATA = Constants.BASE_URL+"/api/displayAdminEvent";
    private RecyclerView recyclerView;

//    private RecyclerView.Adapter adapter;

    private MyAdapterEvent myAdapterEvent;

    private List<Event> eventList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_view_event_activity);


        viewPager = findViewById(R.id.viewPager);
        tabLayout = findViewById(R.id.tabLayout);

        setUpViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


//        recyclerView = findViewById(R.id.recyclerViewListEvent);
//        recyclerView.setHasFixedSize(true);
//        recyclerView.setLayoutManager(new LinearLayoutManager(this));
//
//        eventList = new ArrayList<>();
//
//
//        sharedPreferences = getSharedPreferences("admin", Context.MODE_PRIVATE);
//        id = sharedPreferences.getString("adminID","");
//        email = sharedPreferences.getString("adminEmail","");
//        token = sharedPreferences.getString("adminToken","");
//        Ename = sharedPreferences.getString("adminName","");


//        adapter = new MyAdapterEvent(eventList,this);
//        recyclerView.setAdapter(adapter);




//        loadRecyclerViewData();





//
//
//        mQueue = Volley.newRequestQueue(this);


//        jsonParse();
    }

    private void setUpViewPager(ViewPager viewPager) {

        PagerAdapterAdminEvent pagerAdapter = new PagerAdapterAdminEvent(getSupportFragmentManager());
        pagerAdapter.addFragment(new TabUpComingEventAdmin(),"UPCOMING EVENT");
        pagerAdapter.addFragment(new TabPastEventAdmin(),"PAST EVENT");


        viewPager.setAdapter(pagerAdapter);
    }


//    private void loadRecyclerViewData(){
//        final ProgressDialog progressDialog = new ProgressDialog(this);
//        progressDialog.setMessage("LOADING DATA...");
//        progressDialog.show();
//
//
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_DATA, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//
//                progressDialog.dismiss();
//                try {
//                    JSONArray j = new JSONArray(response);
//
//                    for (int i =0; i<j.length(); i++)
//                    {
//                        JSONObject obj= j.getJSONObject(i);
//
//                        JSONArray obj2 = obj.getJSONArray("categories");
//                        ArrayList category = new ArrayList();
//                        String cate ="";
//                        for(int k =0; k<obj2.length(); k++)
//                        {
//                            JSONObject objCategory = obj2.getJSONObject(k);
//                            String catName =  objCategory.getString("category");
//
//
//                            cate += catName+", ";
//
//
//                        }
//                        Event event = new Event(
//                                obj.getString("id"),
//                                obj.getString("name"),
//                                obj.getString("description"),
//                                obj.getString("date"),
//                                obj.getString("venue"),
//                                obj.getString("location"),
//                                obj.getString("image"),
//                                obj.getString("dated"),
//                                obj.getString("timed"),
//                                cate
//
//
//                        );
//
//                        Log.i("fdsafd",cate);
//                        eventList.add(event);
//
//                    }
//
//                    myAdapterEvent = new MyAdapterEvent(eventList,getApplicationContext());
//                    recyclerView.setAdapter(myAdapterEvent);
//                    myAdapterEvent.setOnItemClickListener(Admin_view_event_activity.this);
//
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                progressDialog.dismiss();
//                Toast.makeText(Admin_view_event_activity.this,error.getMessage(),Toast.LENGTH_LONG).show();
//            }
//        }){
//            @Override
//            protected Map<String,String> getParams(){
//
//                Map<String , String> params = new HashMap<String, String>();
//
//                params.put("user_id",id);
//
//                return params;
//
//            }
//        };
//
//        RequestQueue requestQueue = Volley.newRequestQueue(this);
//        requestQueue.add(stringRequest);
//    }

//    @Override
//    public void onItemClick(int position) {
//        Intent i = new Intent(this, DetailActivity_event_admin.class);
//        Event  clickedItem = eventList.get(position);
//
//        i.putExtra("id",clickedItem.getId());
//        i.putExtra("image",clickedItem.getImage());
//        i.putExtra("name",clickedItem.getName());
//        i.putExtra("description",clickedItem.getDescription());
//        i.putExtra("date",clickedItem.getDate());
//        i.putExtra("dated",clickedItem.getDateddd());
//        i.putExtra("timed",clickedItem.getTimeddd());
//        i.putExtra("venue",clickedItem.getVenue());
//        i.putExtra("location",clickedItem.getLocation());
//
//
//        sharedPreferences = getSharedPreferences("event", Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//
//        editor.putString("event_id",clickedItem.getId());
//        editor.putString("timed",clickedItem.getTimeddd());
//        editor.apply();
//
//        startActivity(i);
//
//
//
//    }


//    private void jsonParse(){
//        String url = "http://192.168.0.100:5000/api/getEvent";
//
//        final JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
//            @Override
//            public void onResponse(JSONArray response) {
//
//                try {
//////                    JsonArray jsonArray = new JsonArray("");
////                    JSONObject resObject  = new JSONObject();
////                    JSONArray jsonArray = resObject.getJSONArray("");
//                    for(int i = 0; i<response.length(); i++)
//                    {
//                        JSONObject emp = response.getJSONObject(i);
//
//                        String first = emp.getString("id");
//                        String name = emp.getString("name");
//                        String description = emp.getString("description");
//                        String date = emp.getString("date");
//                        String venue = emp.getString("venue");
//                        String location = emp.getString("location");
//
//
//                        r.append(first + ", " + name + ", " + description+ ", " + venue + ", " +location + ", "+date+"\n\n");
//
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                error.printStackTrace();
//            }
//        });
//
//        mQueue.add(jsonArrayRequest);
//    }
}
