package com.example.unievent.AdminActivity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.unievent.Model.Constants;
import com.example.unievent.R;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class Admin_update_event extends AppCompatActivity implements DatePickerDialog.OnDateSetListener,TimePickerDialog.OnTimeSetListener {

    String id,image,title,description,date,location,venue;

    final int CODE_GALLERY_REQUEST = 1;
    Bitmap bitmap;
    int day, month , year, hour,minute;
    int dayFinal , monthFinal, yearFinal, hourFinal, minuteFinal;
    String DateParams , paramsDay, paramsMonth ,paramsYear, paramsHour,paramsMinute ;
    ImageView imageUpload;

    String comment,organizer,category;

    Button btn_submit,DateBTN,btnUpload;

    EditText Ename,Edesc,Elocation,Evenue,Ecomment;
    TextView Edate;

    String mdated,mtimed;

    TextView Edated,Etimed;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_update_event);

        sharedPreferences = getSharedPreferences("event", Context.MODE_PRIVATE);
//        event_id = sharedPreferences.getString("event_id","");
        String timeed = sharedPreferences.getString("timed","");

        btn_submit= findViewById(R.id.button_Submit_edit);
        imageUpload = findViewById(R.id.imageUpload);

        Ename = findViewById(R.id.name_event_edit);
        Edesc =findViewById(R.id.name_description_edit);
//        Elocation =findViewById(R.id.name_location_edit);
        Evenue = findViewById(R.id.name_venue_edit);

        Edated = findViewById(R.id.name_dated_edit);
        Etimed = findViewById(R.id.name_timed_edit);
        Ecomment = findViewById(R.id.comment_edit);

        Edate = findViewById(R.id.text_date_edit);


        Intent i = getIntent();
        id = i.getStringExtra("id");
        image = i.getStringExtra("image");
        title = i.getStringExtra("name");
        description = i.getStringExtra("description");
        mdated =i.getStringExtra("dated");
        mtimed =i.getStringExtra("timed");
        date = i.getStringExtra("date");
        location = i.getStringExtra("location");
        venue = i.getStringExtra("venue");
        comment = i.getStringExtra("comment");
        category = i.getStringExtra("category");
        organizer = i.getStringExtra("organizer");


        Ename.setText(title);
        Edesc.setText(description);
//        Elocation.setText(location);
        Evenue.setText(venue);
        Ecomment.setText(comment);

        Etimed.setText(mtimed);
        Edated.setText(mdated);

        Edate.setText(date);

        Picasso.get().load(image).fit().centerInside().into(imageUpload);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateFunction();

            }
        });


        DateBTN = findViewById(R.id.btn_date);

        DateBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                datePicker.show(getSupportFragmentManager(),"date picker");

                Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(Admin_update_event.this,Admin_update_event.this,year,month,day);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

                datePickerDialog.show();
            }
        });


        btnUpload = findViewById(R.id.Btn_image);
        btnUpload.setEnabled(false);
        btnUpload.setVisibility(View.GONE);

        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(Admin_update_event.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        CODE_GALLERY_REQUEST
                );


//                Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                startActivityForResult(galleryIntent,CODE_GALLERY_REQUEST);
            }
        });

    }//end of oncreate









    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == CODE_GALLERY_REQUEST){
            if(grantResults.length> 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent,"Select image"), CODE_GALLERY_REQUEST);

            }
            else{
                Toast.makeText(getApplicationContext(),"you dont have perimission to access gallery ! ",Toast.LENGTH_LONG).show();
            }
            return;
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CODE_GALLERY_REQUEST && resultCode == RESULT_OK && data != null) {
            Uri filePath = data.getData();

            try {
                InputStream inputStream = getContentResolver().openInputStream(filePath);
                bitmap = BitmapFactory.decodeStream(inputStream);
                imageUpload.setImageBitmap(bitmap);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }
    }

    private String imageToString(Bitmap bitmap){

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100, outputStream);
        byte[] imageBytes = outputStream.toByteArray();

        String enccodedImage = Base64.encodeToString(imageBytes,Base64.DEFAULT);
        return enccodedImage;

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
//        Calendar c = Calendar.getInstance();
//        c.set(Calendar.YEAR,year);
//        c.set(Calendar.MONTH,month);
//        c.set(Calendar.DAY_OF_MONTH,dayOfMonth);
//        String currentDateSrting = DateFormat.getDateInstance(DateFormat.FULL).format(c.getTime());
//        textView.setText(currentDateSrting);


        yearFinal = year;
        monthFinal = month + 1;
        dayFinal = dayOfMonth;

        Calendar c = Calendar.getInstance();
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute = c.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(Admin_update_event.this,Admin_update_event.this, hour , minute , DateFormat.is24HourFormat(this));
        timePickerDialog.show();
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

        hourFinal = hourOfDay;
        minuteFinal = minute;

//        Edate.setText("year:" +yearFinal + "\n" +
//                "month:" +monthFinal + "\n" +
//                "day:" +dayFinal + "\n" +
//                "hour:" +hourFinal + "\n" +
//                "minute:" +minuteFinal );



//logic convert date to string
        paramsYear = Integer.toString(yearFinal);
        paramsMonth= Integer.toString(monthFinal);
        paramsDay = Integer.toString(dayFinal);
        paramsHour =Integer.toString(hourFinal);
        paramsMinute = Integer.toString(minuteFinal);

        Edated. setText( paramsDay + "-" + paramsMonth + "-" + paramsYear);
        Etimed .setText (paramsHour + ":" + paramsMinute);

        Edate.setText( paramsYear +"-"+ paramsMonth + "-" + paramsDay +" " + paramsHour+ ":" + paramsMinute) ;


    }












    private void updateFunction()
    {
        String BASE_URL = Constants.BASE_URL+"/api/updateEvent/"+id;
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        StringRequest stringRequest = new StringRequest(Request.Method.PUT, BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(Admin_update_event.this,"update success",Toast.LENGTH_LONG).show();

                Intent i = new Intent(Admin_update_event.this, Admin_profile_activity.class);
                startActivity(i);
                finish();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Admin_update_event.this,error.getMessage(),Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            protected Map<String,String> getParams(){

                Map<String , String> params = new HashMap<String, String>();



                params.put("name",Ename.getText().toString());
                params.put("description",Edesc.getText().toString());
                params.put("date",Edate.getText().toString());
//                params.put("location",Elocation.getText().toString());
                params.put("venue",Evenue.getText().toString());


                params.put("dated",Edated.getText().toString());
                params.put("timed",Etimed.getText().toString());
                params.put("comment",Ecomment.getText().toString());
//                params.put("organizer",organizer)
//                params.put("user_id",id);

//                String imageData = imageToString(bitmap);
//                params.put("image",imageData);

                Log.i("masukla",params.toString());

                return params;

            }
        };
        requestQueue.add(stringRequest);
    }
}
