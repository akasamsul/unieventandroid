package com.example.unievent.AdminActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.unievent.MainActivity;
import com.example.unievent.R;

public class Admin_profile_activity extends AppCompatActivity {

    TextView welcome;

    Button btn_event,btn_announcement,btn_view,btn_view_announcment,btn_logout,test,change_password,edit_profile;
    SharedPreferences sharedPreferences;

    String id,name,email,token;



    TextView textEmail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_profile_activity);

//        test=findViewById(R.id.checkboxtest);
//        test.setVisibility(View.GONE);
//        test.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(Admin_profile_activity.this, UserRegisterCategory.class);
//                startActivity(i);
//            }
//        });
        edit_profile=findViewById(R.id.button5);
        edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Admin_profile_activity.this,admin_edit_profile.class);
                startActivity(i);
            }
        });

        welcome = findViewById(R.id.textView5);

        btn_logout = findViewById(R.id.logout);
        sharedPreferences = getSharedPreferences("admin", Context.MODE_PRIVATE);
        id = sharedPreferences.getString("adminID","");
        email = sharedPreferences.getString("adminEmail","");
        token = sharedPreferences.getString("adminToken","");
        name = sharedPreferences.getString("adminName","");

        welcome.setText("welcome "+name);



        textEmail = findViewById(R.id.admin_email);
        textEmail.setText(email);

        btn_announcement = findViewById(R.id.bt_announcement);
        btn_event = findViewById(R.id.bt_event);
        btn_view = findViewById(R.id.bt_view);
        btn_view_announcment = findViewById(R.id.bt_view_announcement);
        change_password = findViewById(R.id.btn_change);

        change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alt = new AlertDialog.Builder(Admin_profile_activity.this);

                alt.setMessage("Do want change your password ?").setCancelable(false)
                        .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent i = new Intent(Admin_profile_activity.this,admin_change_password.class);
                                startActivity(i);
                            }
                        })
                        .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alertDialog = alt.create();
                alertDialog.setTitle("CHANGE PASSWORD");
                alertDialog.show();


            }
        });

        btn_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Admin_profile_activity.this, Admin_create_event_activity.class);
                startActivity(i);

            }
        });


        btn_announcement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Admin_profile_activity.this, Admin_create_announcement_activity.class);
                startActivity(i);
            }
        });


        btn_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Admin_profile_activity.this, Admin_view_event_activity.class);
                startActivity(i);
            }
        });


        btn_view_announcment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Admin_profile_activity.this, Admin_view_announcement_activity.class);
                startActivity(i);
            }
        });


        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                AlertDialog.Builder alt = new AlertDialog.Builder(Admin_profile_activity.this);

                alt.setMessage("logout ?").setCancelable(false)
                        .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                sharedPreferences = getSharedPreferences("admin", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
//                editor.putString("facebookID","");
//                editor.putString("facebookEmail","");
//                editor.putString("check","");
//                editor.putString("facebookName","");
//                editor.putString("facebookImage","");
//                editor.putString("userResponse","");
//                editor.putBoolean("facebookLogin",false);
//                editor.apply();
                                editor.clear();
                                editor.commit();
                                finish();

                                Intent i = new Intent(Admin_profile_activity.this, MainActivity.class);
                                startActivity(i);
                            }
                        })
                        .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alertDialog = alt.create();
                alertDialog.setTitle("LOGOUT");
                alertDialog.show();



            }
        });
    }
}
