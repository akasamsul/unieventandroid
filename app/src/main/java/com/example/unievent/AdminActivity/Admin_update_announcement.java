package com.example.unievent.AdminActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.unievent.Model.Constants;
import com.example.unievent.R;

import java.util.HashMap;
import java.util.Map;

public class Admin_update_announcement extends AppCompatActivity {

    Button update;
    EditText title,desc;

    String id,name,description;

    String TextTitle, TextDec;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_update_announcement);

        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        name = intent.getStringExtra("title");
        description = intent.getStringExtra("description");

        update=findViewById(R.id.btn_update);

        title = findViewById(R.id.title_update);
        desc = findViewById(R.id.anncmnt_update);


        title.setText(name);
        desc.setText(description);

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check();
            }
        });


    }


    private void check()
    {
        intialize();

        if(!valitation())
        {

            Toast.makeText(Admin_update_announcement.this,"please enter a valid input",Toast.LENGTH_LONG).show();

        }

        else
        {
            updateFunction();

        }
    }

    private void intialize()
    {
        TextTitle = title.getText().toString();
        TextDec = desc.getText().toString();


    }


    private boolean valitation()
    {
        boolean valid = true;
        if(TextTitle.isEmpty())
        {
//            !e.matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+")
            title.setError("cannot be empty");
            valid = false;
        }



        if(TextDec.isEmpty())
        {
            desc.setError("cannot be empty");
            valid = false;
        }



        return valid;
    }



    private void updateFunction()
    {
        String BASE_URL = Constants.BASE_URL+"/api/updateAnnouncement/"+id;
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        StringRequest stringRequest = new StringRequest(Request.Method.PUT, BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(Admin_update_announcement.this,"updated",Toast.LENGTH_LONG).show();
                Intent i = new Intent(Admin_update_announcement.this,Admin_profile_activity.class);
                startActivity(i);
                finish();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Admin_update_announcement.this,error.getMessage(),Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            protected Map<String,String> getParams(){

                Map<String , String> params = new HashMap<String, String>();



                params.put("title",title.getText().toString());
                params.put("description",desc.getText().toString());

                Log.i("masukla",params.toString());

                return params;

            }
        };
        requestQueue.add(stringRequest);
    }

}
