package com.example.unievent.AdminActivity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.unievent.Model.Category;
import com.example.unievent.Model.Constants;
import com.example.unievent.R;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import java.text.DateFormat;
//import java.text.DateFormat;


public class Admin_create_event_activity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private RequestQueue requestQueue;


    String Vtitle,Vvenue,Vdescription;
    ImageView Vimage;

    final int CODE_GALLERY_REQUEST = 1;

    List<Category> selection = new ArrayList<>();
    String newDatarray;

    Bitmap bitmap;

    int day, month , year, hour,minute;
    int dayFinal , monthFinal, yearFinal, hourFinal, minuteFinal;

    String DateParams , paramsDay, paramsMonth ,paramsYear, paramsHour,paramsMinute ;
    ImageView imageUpload;
    TextView textView ;
     Button submit,DateBTN,btnUpload;
     EditText name,desc,location,venue;

    SharedPreferences sharedPreferences;

    String id,email,token,Ename;
    String event_comment;

    TextView comment;

    //separate date and time
    String mdate, mtime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_create_event_activity);


        imageUpload = findViewById(R.id.imageUpload);

        btnUpload = findViewById(R.id.Btn_image);
        venue = findViewById(R.id.name_venue);
//        location = findViewById(R.id.name_location);
        textView = findViewById(R.id.text_date);
        name = findViewById(R.id.name_event);
        desc = findViewById(R.id.name_description);

        DateBTN = findViewById(R.id.btn_date);
        submit = findViewById(R.id.button4);
        comment = findViewById(R.id.event_comment);




        sharedPreferences = getSharedPreferences("admin", Context.MODE_PRIVATE);
        id = sharedPreferences.getString("adminID","");
        email = sharedPreferences.getString("adminEmail","");
        token = sharedPreferences.getString("adminToken","");
        Ename = sharedPreferences.getString("adminName","");


        DateBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                datePicker.show(getSupportFragmentManager(),"date picker");

                Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(Admin_create_event_activity.this,Admin_create_event_activity.this,year,month,day);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();
            }
        });



        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(Admin_create_event_activity.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        CODE_GALLERY_REQUEST
                );


//                Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                startActivityForResult(galleryIntent,CODE_GALLERY_REQUEST);
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {





                postCreateEvent();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == CODE_GALLERY_REQUEST){
            if(grantResults.length> 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent,"Select image"), CODE_GALLERY_REQUEST);

            }
            else{
                Toast.makeText(getApplicationContext(),"you dont have perimission to access gallery ! ",Toast.LENGTH_LONG).show();
            }
            return;
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == CODE_GALLERY_REQUEST && resultCode == RESULT_OK && data != null){
            Uri filePath = data.getData();

            try {
                InputStream inputStream = getContentResolver().openInputStream(filePath);
                bitmap = BitmapFactory.decodeStream(inputStream);
                imageUpload.setImageBitmap(bitmap);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }




    }

    private String imageToString(Bitmap bitmap){

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100, outputStream);
        byte[] imageBytes = outputStream.toByteArray();

        String enccodedImage = Base64.encodeToString(imageBytes,Base64.DEFAULT);
        return enccodedImage;

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
//        Calendar c = Calendar.getInstance();
//        c.set(Calendar.YEAR,year);
//        c.set(Calendar.MONTH,month);
//        c.set(Calendar.DAY_OF_MONTH,dayOfMonth);
//        String currentDateSrting = DateFormat.getDateInstance(DateFormat.FULL).format(c.getTime());
//        textView.setText(currentDateSrting);


        yearFinal = year;
        monthFinal = month + 1;
        dayFinal = dayOfMonth;

        Calendar c = Calendar.getInstance();
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute = c.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(Admin_create_event_activity.this,Admin_create_event_activity.this, hour , minute , DateFormat.is24HourFormat(this));
        timePickerDialog.show();
     }




    private void postCreateEvent()
    {
        Gson gson = new Gson();
        newDatarray = gson.toJson(selection);

        String BASE_URL = Constants.BASE_URL+"/api/createCategoryEvent";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(Admin_create_event_activity.this,"success !",Toast.LENGTH_LONG).show();

                Intent i = new Intent(Admin_create_event_activity.this,Admin_profile_activity.class);
                startActivity(i);
                finish();


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Admin_create_event_activity.this,error.getMessage(),Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            protected Map<String,String> getParams(){

                Map<String , String> params = new HashMap<String, String>();

                params.put("array",newDatarray);
                params.put("name",name.getText().toString().trim());
                params.put("description",desc.getText().toString().trim());
               params.put("date",DateParams.trim());
//                params.put("location",location.getText().toString().trim());
                params.put("venue",venue.getText().toString().trim());
                params.put("user_id",id);

                params.put("dated",mdate);
                params.put("timed",mtime);

                params.put("comment",comment.getText().toString());
                params.put("organizer",Ename);

                String imageData = imageToString(bitmap);
                params.put("image",imageData);


                Log.i("masukla",params.toString());

                return params;

            }
        };
        requestQueue.add(stringRequest);


    }


    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

        hourFinal = hourOfDay;
        minuteFinal = minute;

//        textView.setText("year:" +yearFinal + "\n" +
//                "month:" +monthFinal + "\n" +
//                "day:" +dayFinal + "\n" +
//                "hour:" +hourFinal + "\n" +
//                "minute:" +minuteFinal );

        textView.setText("Date: " +dayFinal+ "/"+monthFinal+"/"+yearFinal + "\n" +
                         "Time: " +hourFinal+":"+minuteFinal);



//logic convert date to string
        paramsYear = Integer.toString(yearFinal);
        paramsMonth= Integer.toString(monthFinal);
        paramsDay = Integer.toString(dayFinal);
        paramsHour =Integer.toString(hourFinal);
        paramsMinute = Integer.toString(minuteFinal);

        mdate = paramsDay + "-" + paramsMonth + "-" + paramsYear;
        mtime = paramsHour + ":" + paramsMinute;

        DateParams = paramsYear +"-"+ paramsMonth + "-" + paramsDay +" " + paramsHour+ ":" + paramsMinute ;

    }


    //this is for category
    public void selectItem(View view)
    {
        boolean checked = ((CheckBox)view).isChecked();

        switch (view.getId())
        {
            case R.id.art :

                if (checked)
                {
                    Category cat = new Category("Arts & Entertainment");

                    selection.add(cat);
                }

                else
                {
                    for(int i = 0 ; i<selection.size(); i++)
                    {
                        if (selection.get(i).getCategory().equals("Arts & Entertainment"))
                        {
                            selection.remove(i);
                        }
                    }
//                    Category cat = new Category("Arts & Entertainment");
//                    selection.remove("Arts & Entertainment");
                }

                break;

            case R.id.beauty :

                if (checked)
                {
                    Category cat = new Category("Beauty, Cosmetic & Personal Care");
                    selection.add(cat);
                }

                else
                {
                    for(int i = 0 ; i<selection.size(); i++)
                    {
                        if (selection.get(i).getCategory().equals("Beauty, Cosmetic & Personal Care"))
                        {
                            selection.remove(i);
                        }
                    }
//                    selection.remove("Education");
                }

                break;

            case R.id.education :

                if (checked)
                {
                    Category cat = new Category("Education");
                    selection.add(cat);
                }

                else
                {
                    for(int i = 0 ; i<selection.size(); i++)
                    {
                        if (selection.get(i).getCategory().equals("Education"))
                        {
                            selection.remove(i);
                        }
                    }
//                    selection.remove("Finance");
                }

                break;

            case R.id.finance :

                if (checked)
                {
                    Category cat = new Category("Finance");
                    selection.add(cat);
                }

                else
                {
                    for(int i = 0 ; i<selection.size(); i++)
                    {
                        if (selection.get(i).getCategory().equals("Finance"))
                        {
                            selection.remove(i);
                        }
                    }
//                    selection.remove("Finance");
                }

                break;

            case R.id.food :

                if (checked)
                {
                    Category cat = new Category("Food & Beverage");
                    selection.add(cat);
                }

                else
                {
                    for(int i = 0 ; i<selection.size(); i++)
                    {
                        if (selection.get(i).getCategory().equals("Food & Beverage"))
                        {
                            selection.remove(i);
                        }
                    }
//                    selection.remove("Finance");
                }

                break;

            case R.id.medical :

                if (checked)
                {
                    Category cat = new Category("Medical & Health");
                    selection.add(cat);
                }

                else
                {
                    for(int i = 0 ; i<selection.size(); i++)
                    {
                        if (selection.get(i).getCategory().equals("Medical & Health"))
                        {
                            selection.remove(i);
                        }
                    }
//                    selection.remove("Finance");
                }

                break;

            case R.id.science :

                if (checked)
                {
                    Category cat = new Category("Science, Technology & Engineering");
                    selection.add(cat);
                }

                else
                {
                    for(int i = 0 ; i<selection.size(); i++)
                    {
                        if (selection.get(i).getCategory().equals("Science, Technology & Engineering"))
                        {
                            selection.remove(i);
                        }
                    }
//                    selection.remove("Finance");
                }

                break;
            case R.id.sport :

                if (checked)
                {
                    Category cat = new Category("Sports & Recreation");
                    selection.add(cat);
                }

                else
                {
                    for(int i = 0 ; i<selection.size(); i++)
                    {
                        if (selection.get(i).getCategory().equals("Sports & Recreation"))
                        {
                            selection.remove(i);
                        }
                    }
//                    selection.remove("Finance");
                }

                break;

            case R.id.outdoor :

                if (checked)
                {
                    Category cat = new Category("Outdoor Recreation");
                    selection.add(cat);
                }

                else
                {
                    for(int i = 0 ; i<selection.size(); i++)
                    {
                        if (selection.get(i).getCategory().equals("Outdoor Recreation"))
                        {
                            selection.remove(i);
                        }
                    }
//                    selection.remove("Finance");
                }

                break;

            case R.id.other :

                if (checked)
                {
                    Category cat = new Category("Other");
                    selection.add(cat);
                }

                else
                {
                    for(int i = 0 ; i<selection.size(); i++)
                    {
                        if (selection.get(i).getCategory().equals("Other"))
                        {
                            selection.remove(i);
                        }
                    }
//                    selection.remove("Finance");
                }

                break;




        }


    }

}
