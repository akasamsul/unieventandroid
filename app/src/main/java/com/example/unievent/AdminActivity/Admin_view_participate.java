package com.example.unievent.AdminActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.unievent.Adapter.MyAdapter_participate;
import com.example.unievent.Details.userparticipate_for_admin;
import com.example.unievent.Model.Constants;
import com.example.unievent.Model.RegisterParticipate;
import com.example.unievent.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Admin_view_participate extends AppCompatActivity implements MyAdapter_participate.OnItemClickListener {

    TextView count;
    private static final String URL_DATA = Constants.BASE_URL+"/api/AdminDisplayUserRegisterEvent";
    int c =0;
    private RecyclerView recyclerView;
    private MyAdapter_participate myAdapter_participate;
    private List<RegisterParticipate> registerParticipateList;

    SharedPreferences sharedPreferences;
    String id,event_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_view_participate);

        count = findViewById(R.id.count);





        sharedPreferences = getSharedPreferences("admin", Context.MODE_PRIVATE);
        id = sharedPreferences.getString("adminID","");

        sharedPreferences = getSharedPreferences("event", Context.MODE_PRIVATE);
        event_id = sharedPreferences.getString("event_id","");


        recyclerView = findViewById(R.id.recyclerViewParticipate);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        registerParticipateList = new ArrayList<>();




        loadRecyclerViewData();



    }



    private void loadRecyclerViewData()
    {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("LOADING DATA...");
        progressDialog.show();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_DATA, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                progressDialog.dismiss();
                try {
                    JSONArray j = new JSONArray(response);

                    for (int i =0; i<j.length(); i++)
                    { c++;
                        JSONObject obj= j.getJSONObject(i);

                        RegisterParticipate event = new RegisterParticipate(
                                obj.getString("name"),
                                obj.getString("matrix"),
                                obj.getString("nophone"),
                                obj.getString("course")

                        );
                        registerParticipateList.add(event);

                    }

                    myAdapter_participate = new MyAdapter_participate(registerParticipateList,getApplicationContext());
                    recyclerView.setAdapter(myAdapter_participate);

                    myAdapter_participate.setOnItemClickListener(Admin_view_participate.this);


                } catch (Exception e) {
                    e.printStackTrace();
                }


                count.setText("Total participate :" + c );

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(Admin_view_participate.this,error.getMessage(),Toast.LENGTH_LONG).show();
            }
        })
        {
            @Override
            protected Map<String,String> getParams(){

                Map<String , String> params = new HashMap<String, String>();



                    params.put("user_id",id);
                     params.put("event_id",event_id);


                return params;

            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    @Override
    public void onItemClick(int position) {

        Intent i = new Intent(this, userparticipate_for_admin.class);
        RegisterParticipate clickedItem = registerParticipateList.get(position);


        i.putExtra("name",clickedItem.getName());
        i.putExtra("matrix",clickedItem.getMatrix());
        i.putExtra("nophone",clickedItem.getPhone());
        i.putExtra("course",clickedItem.getCourse());

        startActivity(i);
    }
}
