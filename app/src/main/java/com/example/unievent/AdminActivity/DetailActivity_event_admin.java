package com.example.unievent.AdminActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.unievent.Model.Constants;
import com.example.unievent.R;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DetailActivity_event_admin extends AppCompatActivity {

    Button btn_delete,btn_update,view;

    String image,id,title,description,date,location,venue,comment,category,organizer;
    String dated,timed;

    TextView TextComment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_event_admin);

        view = findViewById(R.id.view_participate);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(DetailActivity_event_admin.this,Admin_view_participate.class);
                startActivity(i);
            }
        });

        TextComment = findViewById(R.id.comment);

        Intent i = getIntent();
        id = i.getStringExtra("id");
        image = i.getStringExtra("image");
        title = i.getStringExtra("name");
        description = i.getStringExtra("description");
        dated = i.getStringExtra("dated");
        timed = i.getStringExtra("timed");
        date = i.getStringExtra("date");
        location = i.getStringExtra("location");
        venue = i.getStringExtra("venue");
        comment = i.getStringExtra("comment");
        category = i.getStringExtra("category");
        organizer = i.getStringExtra("organizer");

        ImageView imageView = findViewById(R.id.image_detail);

        TextView textViewT = findViewById(R.id.title_detail);
        TextView textViewD = findViewById(R.id.description_detail);
        TextView textViewDate = findViewById(R.id.date_detail);
        TextView textViewVenue = findViewById(R.id.venue_detail);



        DateFormat outputFormat = new SimpleDateFormat("EEE, dd/MM/yyyy hh.mm aa");
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date1 = null;
        try {
            date1 = inputFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outputText = outputFormat.format(date1);

        Picasso.get().load(image).into(imageView);

        textViewT.setText(title);
        textViewD.setText(description);

        textViewDate.setText(outputText);
        textViewVenue.setText(venue);
        TextComment.setText(comment);


        btn_delete = findViewById(R.id.delete);
        btn_update = findViewById(R.id.update);

        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(DetailActivity_event_admin.this, Admin_update_event.class);

                i.putExtra("id",id);
                i.putExtra("image",image);
                i.putExtra("name",title);
                i.putExtra("description",description);
                i.putExtra("timed",timed);
                i.putExtra("dated",dated);
                i.putExtra("date",date);
                i.putExtra("location",location);
                i.putExtra("name",title);
                i.putExtra("venue",venue);
                i.putExtra("comment",comment);
                i.putExtra("category",category);
                i.putExtra("organizer",organizer);



                startActivity(i);

            }
        });


        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                String deteleUrl =  Constants.BASE_URL+"/api/deleteEvent/"+id;
//                Toast.makeText(DetailActivity_event_admin.this,deteleUrl.toString(),Toast.LENGTH_LONG).show();

                AlertDialog.Builder alt = new AlertDialog.Builder(DetailActivity_event_admin.this);

                alt.setMessage("Do you want to delete this event ?").setCancelable(false)
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteEvent();
                    }
                }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog alert = alt.create();
                alert.setTitle("DELETE EVENT");
                alert.show();

            }
        });



    }


    public void deleteEvent()
    {
        String deteleUrl =  Constants.BASE_URL+"/api/deleteEvent/"+id;
//        String BASE_URL = Constants.BASE_URL+"/api/createEvent";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, deteleUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(DetailActivity_event_admin.this,"delete success",Toast.LENGTH_LONG).show();
                Intent i = new Intent(DetailActivity_event_admin.this,Admin_profile_activity.class);
                startActivity(i);
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(Admin_create_event_activity.this,error.getMessage(),Toast.LENGTH_LONG).show();

            }
        });

        requestQueue.add(stringRequest);
    }
}
