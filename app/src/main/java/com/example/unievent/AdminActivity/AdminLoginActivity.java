package com.example.unievent.AdminActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.unievent.Model.Constants;
import com.example.unievent.Model.User;
import com.example.unievent.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AdminLoginActivity extends AppCompatActivity {

    EditText A_email,A_password;

    Button A_login;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_login);

        A_email = findViewById(R.id.email);
        A_password = findViewById(R.id.password);

        A_login = findViewById(R.id.login);

        A_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adminLogin();
            }
        });
    }
    String role;
    private void adminLogin()
    {
        String BASE_URL = Constants.BASE_URL+"/api/adminLogin";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                Toast.makeText(AdminLoginActivity.this,response.toString(),Toast.LENGTH_LONG).show();


                    try {
                        JSONObject resObject  = new JSONObject(response);
                        JSONObject json2 = resObject.getJSONObject("success");    // this will return correct
                        Log.i("masukkal", "-->" + json2.getString("id"));

                        String id;
                        String email;
                        String name;
                        String token;
                        User user  = new User(
                               id = json2.getString("id"),
                                token=  json2.getString("token"),
                                name= json2.getString("name"),
                                email=json2.getString("email"),
                                role = json2.getString("role_type")
                        );

                        new User(name,token,id,email);
                        Log.i("masukkal", "-->" + json2.getString("id"));
                        Log.i("masukkal", "-->" + json2.getString("token"));
                        Log.i("masukkal", "-->" + json2.getString("name"));
                        Log.i("aa",id.toString());

                        sharedPreferences = getSharedPreferences("admin", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("adminID",id);
                        editor.putString("adminEmail",email);
                        editor.putString("adminName",name);
                        editor.putString("adminToken",token);

                        editor.putBoolean("adminLogin",true);
                        editor.apply();




                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }





                    if(role.equals("3"))
                    {
                        Intent i = new Intent(AdminLoginActivity.this, Admin_profile_activity.class);
                        startActivity(i);
                        finish();
                    }
                    else
                    {
                        sharedPreferences = getSharedPreferences("admin", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.clear();

                        Toast.makeText(AdminLoginActivity.this,"wrong password and user name",Toast.LENGTH_LONG).show();
                        Intent i = new Intent(AdminLoginActivity.this, AdminLoginActivity.class);
                        startActivity(i);
                    }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(AdminLoginActivity.this,error.getMessage(),Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            protected Map<String,String> getParams(){

                Map<String , String> params = new HashMap<String, String>();

                params.put("email",A_email.getText().toString().trim());
                params.put("password",A_password.getText().toString().trim());
                return params;

//                sharedPreferences = getSharedPreferences("adminPref", Context.MODE_PRIVATE);
//                SharedPreferences.Editor editor = sharedPreferences.edit();
//                editor.putString("userID",A_email);
//                editor.putString("userEmail",email);
//                //editor.putString("userName",first_name);
//                editor.putString("userName",name);
//                editor.putString("userImage",image_url);
//
//                editor.putString("userResponse",response.getRawResponse());
//
//                editor.putBoolean("userLogin",true);
//                editor.apply();

            }
        };
        requestQueue.add(stringRequest);

    }


    @Override
    public void finish()
    {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    }
}
