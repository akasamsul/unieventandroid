package com.example.unievent.AdminActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.unievent.Model.Constants;
import com.example.unievent.R;

import java.util.HashMap;
import java.util.Map;

public class Admin_create_announcement_activity extends AppCompatActivity {

    EditText announcemnt,title;
    Button sumbit;

    String id,email,token,name;

    String TextTitle, TextDec;

    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_create_announcement_activity);

        title=findViewById(R.id.title);
        announcemnt = findViewById(R.id.anncmnt);
        sumbit = findViewById(R.id.btnSUBMIT);

        sumbit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                send();
            }
        });

        sharedPreferences = getSharedPreferences("admin", Context.MODE_PRIVATE);
        id = sharedPreferences.getString("adminID","");
        email = sharedPreferences.getString("adminEmail","");
        token = sharedPreferences.getString("adminToken","");
        name = sharedPreferences.getString("adminName","");

    }


    private void send()
    {
        intialize();

        if(!valitation())
        {

            Toast.makeText(Admin_create_announcement_activity.this,"please enter a valid input",Toast.LENGTH_LONG).show();

        }

        else
        {
            postAnnouncement();

        }
    }

    private void intialize()
    {
        TextTitle = title.getText().toString();
        TextDec = announcemnt.getText().toString();


    }


    private boolean valitation()
    {
        boolean valid = true;
        if(TextTitle.isEmpty())
        {
//            !e.matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+")
            title.setError("cannot be empty");
            valid = false;
        }



        if(TextDec.isEmpty())
        {
            announcemnt.setError("cannot be empty");
            valid = false;
        }



        return valid;
    }



    private void postAnnouncement()
    {

            String BASE_URL = Constants.BASE_URL+"/api/CreateAnnouncement";
            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

            StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Toast.makeText(Admin_create_announcement_activity.this,"success, your news will only appear for 3 days",Toast.LENGTH_LONG).show();
                    Intent i = new Intent(Admin_create_announcement_activity.this,Admin_profile_activity.class);
                    startActivity(i);
                    finish();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(Admin_create_announcement_activity.this,error.getMessage(),Toast.LENGTH_LONG).show();

                }
            }){
                @Override
                protected Map<String,String> getParams(){

                    Map<String , String> params = new HashMap<String, String>();

                    params.put("title",title.getText().toString());
                    params.put("description",announcemnt.getText().toString());
                    params.put("user_id",id);

                    Log.i("masukla",params.toString());

                    return params;

                }
            };
            requestQueue.add(stringRequest);




    }
}
