package com.example.unievent;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.unievent.Adapter.MyAdapter;
import com.example.unievent.Model.ListItem;
import com.facebook.login.LoginManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class recyclerPage extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;

    private List<ListItem> listItems;

    Button btn,next2;
    SharedPreferences sharedPreferences;

    String response;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_page);

        btn = findViewById(R.id.logout);

        sharedPreferences = getSharedPreferences("facebookDisplay", Context.MODE_PRIVATE);
        response = sharedPreferences.getString("userResponse","");

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        listItems = new ArrayList<>();




//        Bundle inBundle = getIntent().getExtras();
//        String response = inBundle.get("response").toString(); // ini tade final pun tape




//        final String name = inBundle.get("nama_dpn").toString();
//        final String belakang = inBundle.get("nama_blkng").toString();
//        final String id = inBundle.get("id").toString();

        //pas

        try {
            JSONObject resObject  = new JSONObject(response);
            JSONObject json2 = resObject.getJSONObject("likes");    // this will return correct
            JSONArray jsonArray = json2.getJSONArray("data");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                Log.i("ID", "-->" + jsonObject.getString("id"));
                Log.i("ID", "-->" + jsonObject.getString("category"));


                ListItem item = new ListItem(jsonObject.getString("category"),jsonObject.getString("id"));
                listItems.add(item);

            }
        }

        catch (JSONException e)
        {
            e.printStackTrace();
        }

        adapter = new MyAdapter(listItems,getApplicationContext());
        recyclerView.setAdapter(adapter);





        btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {


                sharedPreferences = getSharedPreferences("facebookDisplay", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
//                editor.putString("facebookID","");
//                editor.putString("facebookEmail","");
//                editor.putString("check","");
//                editor.putString("facebookName","");
//                editor.putString("facebookImage","");
//                editor.putString("userResponse","");
//                editor.putBoolean("facebookLogin",false);
//                editor.apply();
                editor.clear();
                editor.commit();
                finish();

                LoginManager.getInstance().logOut();
                Intent login = new Intent(recyclerPage.this,MainActivity.class);
                startActivity(login);
                finish();
            }
        });

    }
}
