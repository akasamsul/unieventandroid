package com.example.unievent;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.unievent.Model.Category;
import com.example.unievent.Model.Constants;
import com.example.unievent.UserActivity.UserProfileActivity;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserRegisterCategory extends AppCompatActivity {

    Spinner spinner1,spinner2, spinner3, spinner4;
    //baru
    Spinner spinner5,spinner6,spinner7,spinner8,spinner9,spinner10;

    ArrayList<String> spinner1Text = new ArrayList<>();
    ArrayList<String> spinner2Text = new ArrayList<>();
    ArrayList<String> spinner3Text = new ArrayList<>();
    ArrayList<String> spinner4Text = new ArrayList<>();
    //baru
    ArrayList<String> spinner5Text = new ArrayList<>();
    ArrayList<String> spinner6Text = new ArrayList<>();
    ArrayList<String> spinner7Text = new ArrayList<>();
    ArrayList<String> spinner8Text = new ArrayList<>();
    ArrayList<String> spinner9Text = new ArrayList<>();
    ArrayList<String> spinner10Text = new ArrayList<>();

    List<Category> selection = new ArrayList<>();
    String newDatarray;

    Button reset, save;
    TextView addCategory,lessCategory;



    //
    SharedPreferences sharedPreferences;
    String id,email,name,token,ucheck;
    String fid,fbid,femail,fname,fcheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_register_category);


        sharedPreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        id = sharedPreferences.getString("userID","");
        email = sharedPreferences.getString("userEmail","");
        name = sharedPreferences.getString("userName","");
        token = sharedPreferences.getString("userToken","");
        ucheck = sharedPreferences.getString("check","");

        sharedPreferences = getSharedPreferences("userFacebook", Context.MODE_PRIVATE);
        fid = sharedPreferences.getString("ID","");
        femail = sharedPreferences.getString("fbEmail","");
        fname = sharedPreferences.getString("fbName","");
        fcheck = sharedPreferences.getString("check","");





        spinner1 = findViewById(R.id.spinner1);
        spinner2 = findViewById(R.id.spinner2);
        spinner3 = findViewById(R.id.spinner3);
        spinner4 = findViewById(R.id.spinner4);
        //baru
        spinner5 = findViewById(R.id.spinner5);
        spinner6 = findViewById(R.id.spinner6);
        spinner7 = findViewById(R.id.spinner7);
        spinner8 = findViewById(R.id.spinner8);
        spinner9 = findViewById(R.id.spinner9);
        spinner10 = findViewById(R.id.spinner10);


        lessCategory = findViewById(R.id.lessCategoryText);
        addCategory = findViewById(R.id.addCategoryText);

        addCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addCategory.setVisibility(View.INVISIBLE);
                lessCategory.setVisibility(View.VISIBLE);

                spinner5.setVisibility(View.VISIBLE);
                spinner6.setVisibility(View.VISIBLE);
                spinner7.setVisibility(View.VISIBLE);
                spinner8.setVisibility(View.VISIBLE);
                spinner9.setVisibility(View.VISIBLE);
                spinner10.setVisibility(View.VISIBLE);

            }
        });


        lessCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addCategory.setVisibility(View.VISIBLE);
                lessCategory.setVisibility(View.INVISIBLE);


                spinner5.setVisibility(View.INVISIBLE);
                spinner6.setVisibility(View.INVISIBLE);
                spinner7.setVisibility(View.INVISIBLE);
                spinner8.setVisibility(View.INVISIBLE);
                spinner9.setVisibility(View.INVISIBLE);
                spinner10.setVisibility(View.INVISIBLE);
            }
        });

        spinner2.setEnabled(false);
        spinner3.setEnabled(false);
        spinner4.setEnabled(false);
        //baru
        spinner5.setEnabled(false);
        spinner6.setEnabled(false);
        spinner7.setEnabled(false);
        spinner8.setEnabled(false);
        spinner9.setEnabled(false);
        spinner10.setEnabled(false);




        reset= findViewById(R.id.reset);
        save = findViewById(R.id.save);

        spinnerInitalizeValue();

        ArrayAdapter<String> spinner1Adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, spinner1Text);
        ArrayAdapter<String> spinner2Adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, spinner2Text);
        ArrayAdapter<String> spinner3Adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, spinner3Text);
        ArrayAdapter<String> spinner4Adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, spinner4Text);
        //baru
        ArrayAdapter<String> spinner5Adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, spinner5Text);
        ArrayAdapter<String> spinner6Adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, spinner6Text);
        ArrayAdapter<String> spinner7Adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, spinner7Text);
        ArrayAdapter<String> spinner8Adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, spinner8Text);
        ArrayAdapter<String> spinner9Adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, spinner9Text);
        ArrayAdapter<String> spinner10Adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, spinner10Text);


        spinner1.setAdapter(spinner1Adapter);
        spinner2.setAdapter(spinner2Adapter);
        spinner3.setAdapter(spinner3Adapter);
        spinner4.setAdapter(spinner4Adapter);
        //baru
        spinner5.setAdapter(spinner5Adapter);
        spinner6.setAdapter(spinner6Adapter);
        spinner7.setAdapter(spinner7Adapter);
        spinner8.setAdapter(spinner8Adapter);
        spinner9.setAdapter(spinner9Adapter);
        spinner10.setAdapter(spinner10Adapter);




        //


        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (!spinner1.getSelectedItem().equals("choose item"))
                {
                    spinner2Text.remove(position);
                    spinner3Text.remove(position);
                    spinner4Text.remove(position);
                    //baru
                    spinner5Text.remove(position);
                    spinner6Text.remove(position);
                    spinner7Text.remove(position);
                    spinner8Text.remove(position);
                    spinner9Text.remove(position);
                    spinner10Text.remove(position);

                    spinner1.setEnabled(false);
                    spinner2.setEnabled(true);

                    Category cat = new Category(spinner1.getSelectedItem().toString(),25);
                    selection.add(cat);
//                    selection.put(spinner1.getSelectedItem().toString(),40);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!spinner2.getSelectedItem().equals("choose item"))
                {
                    spinner3Text.remove(position);
                    spinner4Text.remove(position);
                    //baru
                    spinner5Text.remove(position);
                    spinner6Text.remove(position);
                    spinner7Text.remove(position);
                    spinner8Text.remove(position);
                    spinner9Text.remove(position);
                    spinner10Text.remove(position);


                    spinner2.setEnabled(false);
                    spinner3.setEnabled(true);

                    Category cat = new Category(spinner2.getSelectedItem().toString(),20);
                    selection.add(cat);

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinner3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(!spinner3.getSelectedItem().equals("choose item"))
                {
                    spinner4Text.remove(position);
                    //baru
                    spinner5Text.remove(position);
                    spinner6Text.remove(position);
                    spinner7Text.remove(position);
                    spinner8Text.remove(position);
                    spinner9Text.remove(position);
                    spinner10Text.remove(position);


                    spinner3.setEnabled(false);
                    spinner4.setEnabled(true);

                    Category cat = new Category(spinner3.getSelectedItem().toString(),15);
                    selection.add(cat);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinner4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!spinner4.getSelectedItem().equals("choose item"))
                {
                    //baru
                    spinner5Text.remove(position);
                    spinner6Text.remove(position);
                    spinner7Text.remove(position);
                    spinner8Text.remove(position);
                    spinner9Text.remove(position);
                    spinner10Text.remove(position);


                    spinner4.setEnabled(false);
                    spinner5.setEnabled(true);

                    Category cat = new Category(spinner4.getSelectedItem().toString(),13);
                    selection.add(cat);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinner5.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!spinner5.getSelectedItem().equals("choose item"))
                {
                    //baru

                    spinner6Text.remove(position);
                    spinner7Text.remove(position);
                    spinner8Text.remove(position);
                    spinner9Text.remove(position);
                    spinner10Text.remove(position);


                    spinner5.setEnabled(false);
                    spinner6.setEnabled(true);

                    Category cat = new Category(spinner5.getSelectedItem().toString(),10);
                    selection.add(cat);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinner6.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!spinner6.getSelectedItem().equals("choose item"))
                {
                    //baru
                    spinner7Text.remove(position);
                    spinner8Text.remove(position);
                    spinner9Text.remove(position);
                    spinner10Text.remove(position);


                    spinner6.setEnabled(false);
                    spinner7.setEnabled(true);

                    Category cat = new Category(spinner6.getSelectedItem().toString(),6);
                    selection.add(cat);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinner7.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!spinner7.getSelectedItem().equals("choose item"))
                {
                    //baru

                    spinner8Text.remove(position);
                    spinner9Text.remove(position);
                    spinner10Text.remove(position);


                    spinner7.setEnabled(false);
                    spinner8.setEnabled(true);

                    Category cat = new Category(spinner7.getSelectedItem().toString(),5);
                    selection.add(cat);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner8.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!spinner8.getSelectedItem().equals("choose item"))
                {
                    //baru
                    spinner9Text.remove(position);
                    spinner10Text.remove(position);


                    spinner8.setEnabled(false);
                    spinner9.setEnabled(true);

                    Category cat = new Category(spinner8.getSelectedItem().toString(),3);
                    selection.add(cat);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner9.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!spinner9.getSelectedItem().equals("choose item"))
                {
                    //baru
                    spinner10Text.remove(position);


                    spinner9.setEnabled(false);
                    spinner10.setEnabled(true);

                    Category cat = new Category(spinner9.getSelectedItem().toString(),2);
                    selection.add(cat);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinner10.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!spinner10.getSelectedItem().equals("choose item"))
                {



                    spinner10.setEnabled(false);


                    Category cat = new Category(spinner10.getSelectedItem().toString(),1);
                    selection.add(cat);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });






        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinner1Text.clear();
                spinner2Text.clear();
                spinner3Text.clear();
                spinner4Text.clear();
                //baru
                spinner5Text.clear();
                spinner6Text.clear();
                spinner7Text.clear();
                spinner8Text.clear();
                spinner9Text.clear();
                spinner10Text.clear();

                spinnerInitalizeValue();

                spinner1.setSelection(0);
                spinner2.setSelection(0);
                spinner3.setSelection(0);
                spinner4.setSelection(0);
                //baru
                spinner5.setSelection(0);
                spinner6.setSelection(0);
                spinner7.setSelection(0);
                spinner8.setSelection(0);
                spinner9.setSelection(0);
                spinner10.setSelection(0);


                spinner1.setEnabled(true);
                spinner2.setEnabled(false);
                spinner3.setEnabled(false);
                spinner4.setEnabled(false);
                //baru
                spinner5.setEnabled(false);
                spinner6.setEnabled(false);
                spinner7.setEnabled(false);
                spinner8.setEnabled(false);
                spinner9.setEnabled(false);
                spinner10.setEnabled(false);

                selection.clear();

            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Gson gson = new Gson();
//                newDatarray = gson.toJson(selection);
//                Log.i("test",newDatarray.toString());


                postUserCategoryRegister();

                Intent i = new Intent(UserRegisterCategory.this, UserProfileActivity.class);
                startActivity(i);
                finish();
            }
        });


    }//end of onCrete




    public void spinnerInitalizeValue()
    {
        spinner1Text.add(0,"choose item");
        spinner1Text.add(1, "Arts & Entertainment");
        spinner1Text.add(2, "Beauty, Cosmetic & Personal Care");
        spinner1Text.add(3, "Education");
        spinner1Text.add(4, "Finance");
        spinner1Text.add(5, "Food & Beverage");
        spinner1Text.add(6, "Medical & Health");
        spinner1Text.add(7, "Science, Technology & Engineering");
        spinner1Text.add(8, "Sports & Recreation");
        spinner1Text.add(9, "Outdoor Recreation");
        spinner1Text.add(10, "Other");



        spinner2Text.add(0,"choose item");
        spinner2Text.add(1, "Arts & Entertainment");
        spinner2Text.add(2, "Beauty, Cosmetic & Personal Care");
        spinner2Text.add(3, "Education");
        spinner2Text.add(4, "Finance");
        spinner2Text.add(5, "Food & Beverage");
        spinner2Text.add(6, "Medical & Health");
        spinner2Text.add(7, "Science, Technology & Engineering");
        spinner2Text.add(8, "Sports & Recreation");
        spinner2Text.add(9, "Outdoor Recreation");
        spinner2Text.add(10, "Other");



        spinner3Text.add(0,"choose item");
        spinner3Text.add(1, "Arts & Entertainment");
        spinner3Text.add(2, "Beauty, Cosmetic & Personal Care");
        spinner3Text.add(3, "Education");
        spinner3Text.add(4, "Finance");
        spinner3Text.add(5, "Food & Beverage");
        spinner3Text.add(6, "Medical & Health");
        spinner3Text.add(7, "Science, Technology & Engineering");
        spinner3Text.add(8, "Sports & Recreation");
        spinner3Text.add(9, "Outdoor Recreation");
        spinner3Text.add(10, "Other");



        spinner4Text.add(0,"choose item");
        spinner4Text.add(1, "Arts & Entertainment");
        spinner4Text.add(2, "Beauty, Cosmetic & Personal Care");
        spinner4Text.add(3, "Education");
        spinner4Text.add(4, "Finance");
        spinner4Text.add(5, "Food & Beverage");
        spinner4Text.add(6, "Medical & Health");
        spinner4Text.add(7, "Science, Technology & Engineering");
        spinner4Text.add(8, "Sports & Recreation");
        spinner4Text.add(9, "Outdoor Recreation");
        spinner4Text.add(10, "Other");


        //baru

        spinner5Text.add(0,"choose item");
        spinner5Text.add(1, "Arts & Entertainment");
        spinner5Text.add(2, "Beauty, Cosmetic & Personal Care");
        spinner5Text.add(3, "Education");
        spinner5Text.add(4, "Finance");
        spinner5Text.add(5, "Food & Beverage");
        spinner5Text.add(6, "Medical & Health");
        spinner5Text.add(7, "Science, Technology & Engineering");
        spinner5Text.add(8, "Sports & Recreation");
        spinner5Text.add(9, "Outdoor Recreation");
        spinner5Text.add(10, "Other");

        spinner6Text.add(0,"choose item");
        spinner6Text.add(1, "Arts & Entertainment");
        spinner6Text.add(2, "Beauty, Cosmetic & Personal Care");
        spinner6Text.add(3, "Education");
        spinner6Text.add(4, "Finance");
        spinner6Text.add(5, "Food & Beverage");
        spinner6Text.add(6, "Medical & Health");
        spinner6Text.add(7, "Science, Technology & Engineering");
        spinner6Text.add(8, "Sports & Recreation");
        spinner6Text.add(9, "Outdoor Recreation");
        spinner6Text.add(10, "Other");

        spinner7Text.add(0,"choose item");
        spinner7Text.add(1, "Arts & Entertainment");
        spinner7Text.add(2, "Beauty, Cosmetic & Personal Care");
        spinner7Text.add(3, "Education");
        spinner7Text.add(4, "Finance");
        spinner7Text.add(5, "Food & Beverage");
        spinner7Text.add(6, "Medical & Health");
        spinner7Text.add(7, "Science, Technology & Engineering");
        spinner7Text.add(8, "Sports & Recreation");
        spinner7Text.add(9, "Outdoor Recreation");
        spinner7Text.add(10, "Other");

        spinner8Text.add(0,"choose item");
        spinner8Text.add(1, "Arts & Entertainment");
        spinner8Text.add(2, "Beauty, Cosmetic & Personal Care");
        spinner8Text.add(3, "Education");
        spinner8Text.add(4, "Finance");
        spinner8Text.add(5, "Food & Beverage");
        spinner8Text.add(6, "Medical & Health");
        spinner8Text.add(7, "Science, Technology & Engineering");
        spinner8Text.add(8, "Sports & Recreation");
        spinner8Text.add(9, "Outdoor Recreation");
        spinner8Text.add(10, "Other");

        spinner9Text.add(0,"choose item");
        spinner9Text.add(1, "Arts & Entertainment");
        spinner9Text.add(2, "Beauty, Cosmetic & Personal Care");
        spinner9Text.add(3, "Education");
        spinner9Text.add(4, "Finance");
        spinner9Text.add(5, "Food & Beverage");
        spinner9Text.add(6, "Medical & Health");
        spinner9Text.add(7, "Science, Technology & Engineering");
        spinner9Text.add(8, "Sports & Recreation");
        spinner9Text.add(9, "Outdoor Recreation");
        spinner9Text.add(10, "Other");

        spinner10Text.add(0,"choose item");
        spinner10Text.add(1, "Arts & Entertainment");
        spinner10Text.add(2, "Beauty, Cosmetic & Personal Care");
        spinner10Text.add(3, "Education");
        spinner10Text.add(4, "Finance");
        spinner10Text.add(5, "Food & Beverage");
        spinner10Text.add(6, "Medical & Health");
        spinner10Text.add(7, "Science, Technology & Engineering");
        spinner10Text.add(8, "Sports & Recreation");
        spinner10Text.add(9, "Outdoor Recreation");
        spinner10Text.add(10, "Other");
    }


    public void postUserCategoryRegister()
    {
        Gson gson = new Gson();
        newDatarray = gson.toJson(selection);

        String BASE_URL = Constants.BASE_URL+"/api/createUserCategory";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());

        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                Toast.makeText(UserRegisterCategory.this,"ss",Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(UserRegisterCategory.this,error.getMessage(),Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            protected Map<String,String> getParams(){

                Map<String , String> params = new HashMap<String, String>();

                if(fcheck.equals("login2"))
                {
                    params.put("user_id",fid);
                    params.put("array",newDatarray);
                }

                else
                {
                    params.put("user_id",id);
                    params.put("array",newDatarray);
                }

//                params.put("array",newDatarray);
//                params.put("user_id","2");

                Log.i("masukla",params.toString());

                return params;

            }
        };

        requestQueue.add(stringRequest);



    }

}//end  of activity
